
e3-piezodriver
======
ESS Site-specific EPICS IOC Application : PiezoDriver

# Description
For the need of the Piezo Compensation System, which is used to deal with Lorentz Force detuning and microphonics, the dedicated piezo controller was designed and produced. It is based on RTM Carrier module and it is compatible with MicroTCA.4 standard.
This README.md presents E3 module with IOC to control piezo elements.

More detailed description is provdied [here](https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HAR&title=Piezo-driver+integration+-+IOC)

## Requirements

- Xilinx DMA/Bridge Subsystem for PCI Express (XDMA) kernel module

An [rpm package](https://gitlab.esss.lu.se/ics-infrastructure/rpms/xdma-dkms) is available in the rpm-ics [repository](https://artifactory.esss.lu.se/ui/native/rpm-ics/centos/7/x86_64).
```sh
$ yum install xdma-dkms
```

## EPICS dependencies
- xdma
- asyn

## Installation

```sh
$ make build
$ make install
```

## GUI operator panels
In opi directory, there are operator panles in .bob format. Launch them in Phoebus and adapt macros definitions in `main_menu` window.

![Main window](opi/images/main_menu.PNG?raw=true)

From this level, there are two accessible windows: Channel A and Channel B.

![Channel window](opi/images/channel_A.PNG?raw=true)

Both A and B panels are divided into two sections: Actuator and Sensor.
First is used to set shape of waveform for piezo actuators and decide how the signal will be triggered. Sensor is responsible for data acquistion settings.

### Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Author Information
In case of problems contact with kklys@mail.dmcs.pl
