############################
### general settings
############################

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","20000000")

############################
### load piezo configuration
############################
require piezodriver

epicsEnvSet("P", "PiezoDriver:")
epicsEnvSet("R", "main:")
epicsEnvSet("PORT", "piezoPort")
epicsEnvSet("DEVICE_NAME","/dev/xdma_pcd1")

iocshLoad("$(piezodriver_DIR)/piezo.iocsh", "P=$(P), R=$(R), PORT=$(PORT), DEVICE_NAME=$(DEVICE_NAME), TIMEOUT=1")

iocInit()
