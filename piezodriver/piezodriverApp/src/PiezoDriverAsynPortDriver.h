#include "asynPortDriver.h"

#include "libpcd.h"
#include "PiezoActuator.h"
#include "PiezoSensor.h"
#include "DiagnosticProcessor.h"

#include <pthread.h>
#include <string.h>
#include <epicsEvent.h>

const int ACT_BUFF = 16384;
const int SEN_BUFF = 8192;
const double ACTUATOR_GAIN = 0x7FFF/187.4;
const int CHAN_A = 0;
const int CHAN_B = 1;


class PiezoDriverAsynPortDriver: public asynPortDriver
{
public:
  PiezoDriverAsynPortDriver(const char *portName, int maxPoints, const char *devName);
  ~PiezoDriverAsynPortDriver();

  asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
  asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
  asynStatus writeFloat32Array(asynUser *pasynUser, epicsFloat32 * value, size_t nElements);

  // second thread reacting on flag from 1st thread
  void PiezoDriverIrq(void);
  void PiezoDriverDiag(void);
  void PiezoDriverMonitor(void);

  // terminate all threads when exit
  void TerminateAllThreads(void);
  // Print error message
  void printError(int code);
  // Set alarm statuses
  void actAlarmProcedure(int result, int channel);
  void sensAlarmProcedure(int result);
  // Set limits
  void setLimits(int down, int up, int* var);
  void setLimits(int down, int up, double* var);
  // Initialize acquisition
  void initAcquisition();

  bool initializeDevice();
  void configureActuators();
  void configureSensor();

  // Arrays for data loaded from file size equals to buffor size
  float array_a[ACT_BUFF];
  float array_a_y[ACT_BUFF];
  float array_b[ACT_BUFF];
  float array_b_y[ACT_BUFF];

  // number of samples loaded from file
  int number_of_samples_a;
  int number_of_samples_b;

  //
  epicsFloat32 **pvData;

protected:
  //flags
  // Channels params flag
  bool P_CHANNELS_needs_update;
  // Sensor params flag
  bool P_SENSOR_needs_update;
  // Sensor configuration flag
  bool P_SENS_CONFIG_needs_update;
  // debug flag
  bool P_TRIG_PER_SEC_needs_update;
  // switch channel state flag
  bool P_CHAN_A_SWITCH_needs_update;
  bool P_CHAN_B_SWITCH_needs_update;
  // actuators params flag
  bool P_ACTUATORS_needs_update;
  // wave parameters channel A and B, loading to Piezo Driver
  bool P_CHAN_A_WAVE_needs_update;
  bool P_CHAN_B_WAVE_needs_update;
  // waveform to be presented from PiezoDriver
  bool P_WAVEFORM_needs_update;
  // flag for capturing waveform mode
  bool P_SENS_CAPTURING_MODE_needs_update;
  // flags for preview of arrays channel A and B
  bool P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update;
  bool P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update;
  // flags for updating parameters of waves channel A and B
  bool P_CHAN_A_WAVE_PARAMS_needs_update;
  bool P_CHAN_B_WAVE_PARAMS_needs_update;
  // flags for trigger 5 sec
  bool time_start;
  bool time_pass;
  // flags for loading data from file to piezo driver
  bool P_CHAN_A_WAVE_LOAD_PD_needs_update;
  bool P_CHAN_B_WAVE_LOAD_PD_needs_update;
  // flag for resetting protection circuit
  bool P_CHAN_A_RST_PROT_needs_update;
  bool P_CHAN_B_RST_PROT_needs_update;

  // Actuator channel A and B  params
  // pcd-tool -p
  // pcd-tool -d
  // pcd-tool -t -a
  // pcd-tool -r
  // pcd-tool -l
  struct
  {
    int TRIG_DELAY_RD;
    int TRIG_OCCUR_RD;
    int TRIG_AUTO_REARM_RD;
    int TRIG_STATE_RD;
    int TRIG_DELAY_SP;
    int TRIG_STATE_SP;
    int TRIG_AUTO_REARM_SP;
    int ALARM_RD;
    int ALARM_OCCUR_RD;
    int RST_PROT_SP;
    int RST_PROT_RD;
    int REFRESH_RD;
    int REFRESH_PARAMS_RD;
  } P_CHAN_A, P_CHAN_B;
  int P_ACT_TRIGGER_PER_LINE_RD;
  int P_ACT_CLEAR_LINE_SP;
  int P_ACT_CLEAR_LINE_RD;
  int P_ACT_TRIGGER_LINE_SP;
  int P_ACT_TRIGGER_LINE_RD;
  int P_ACT_EVENT_NUM_SP;
  int P_ACT_EVENT_NUM_RD;

  // Sensor channel params
  // variables with RD pcd-tool -p
  // pcd-tool -A
  // pcd-tool -P
  // pcd-tool -R
  // pcd-tool -S
  // pcd-tool -T
  // pcd-tool -L
  int P_SENS_PRE_TRIG_RD;
  int P_SENS_PRE_TRIG_SP;
  int P_SENS_SAMPLE_COUNT_RD;
  int P_SENS_DELAY_RD;
  int P_SENS_DELAY_SP;
  int P_SENS_SAMPLES_TO_DROP_RD;
  int P_SENS_SAMPLES_TO_DROP_SP;
  int P_SENS_TRIG_OCCUR_RD;
  int P_SENS_TRIG_STATE_RD;
  int P_SENS_TRIG_STATE_SP;
  int P_SENS_TRIG_AUTO_REARM_RD;
  int P_SENS_TRIG_AUTO_REARM_SP;
  int P_SENS_SWITCH_SP;
  int P_SENS_CAPTURING_MODE_SP;
  int P_SENS_ALARM_RD;
  int P_SENS_ALARM_GEN_RD;
  int P_SENS_ALARM_OCCUR_RD;
  int P_SENS_ALARM_OCCUR_GEN_RD;
  int P_SENS_SAVING_TYPE_SP;
  int P_SENS_TIMEOUT_RD;
  int P_SENS_TIMEOUT_SP;
  int P_SENS_TRIGGER_LINE_SP;
  int P_SENS_CLEAR_LINE_SP;
  int P_SENS_EVENT_NUM_SP;
  int P_SENS_TRIGGER_LINE_RD;
  int P_SENS_CLEAR_LINE_RD;
  int P_SENS_EVENT_NUM_RD;
  int P_SENS_TRIGGER_PER_LINE_RD;

  // pcd-tool -p
  int P_TRIG_LEN_RD;
  int P_TRIG_CNT_RD;
  // pcd tool -S
  int P_SENS_SAMPLE_COUNT_SP;

  // debug
  // pcd-tool -2
  int P_TRIG_PER_SEC_RD;
  int P_TRIG_PER_SEC_SP;

  // switch channel
  // pcd-tool -m
  struct
  {
    int ACT_RD;
    int DES_RD;
    int SP;
  } P_CHAN_A_SWITCH, P_CHAN_B_SWITCH;

  // pcd-tool -x
  // path to dma file
  int P_DMA_FILE_PATH_SP;
  int P_DMA_FILE_PATH_RD;

  // dev connected
  int P_DEV_CON_RD;

  // pcd -tool -w
  struct
  {
    int F_SP;
    int F_RD;
    int A_SP;
    int A_RD;
    int OFFSET_SP;
    int OFFSET_RD;
    int TRP_TR_SP;
    int TRP_TR_RD;
    int TRP_TF_SP;
    int TRP_TF_RD;
    int ARRAY_RD;
    int ARRAY_X_RD;
    int TYPE_SP;
    int TYPE_RD;
    int SWITCH_SP;
    int SWITCH_PREVIEW_SP;
    int PREVIEW_TYPE_SP;
    int PREVIEW_TYPE_RD;
    int FILE_RD;
    int FILE_SWITCH_SP;
    int LOAD_SAMPLES_SUGG_RD;
    int LOAD_PD_SP;
    int LOAD_PD_RD;
  } P_CHAN_A_WAVE, P_CHAN_B_WAVE;

  //waveforms
  //pcd-tool -N, -G, -C
  int P_WAVE_CH_A_RD;
  int P_WAVE_CH_B_RD;
  int P_WAVE_CH_AB_RD;
  int P_WAVE_CH_C_RD;
  int P_WAVE_CH_D_RD;
  int P_WAVE_CH_CD_RD;

  int P_WAVE_BUFF_SP;
  char *deviceName;

  // diagnostic processor
  struct
  {
    int MODE_RD;
    int VOLT_AC_RD;
    int VOLT_DC_RD;
    int VOLT_RMS_RD;
    int CUR_AC_RD;
    int CUR_DC_RD;
    int CUR_RMS_RD;
    int STATUS_RD;
    int DETAILED_STATUS_RD;
    int POW_AMP_RD;
    int DETAILED_POW_AMP_RD;
    int RESISTOR_RD;
    int TEMP_RD;
  } P_CHAN_A_DIAG_CPU, P_CHAN_B_DIAG_CPU;

private:
  int trig_cnt1, trig_cnt2;
  uint8_t terminateWorkers;

  epicsMutexId terminateWorkersLock;

  uint8_t ackFromWorkersCount;
  pthread_mutex_t ackFromWorkersLock;
  pthread_cond_t ackFromWorkersCond;
  pthread_cond_t ackForThread;

  // device
  xildev *dev = NULL;
  bool deviceConnected;
  piezoActuator chanA;
  piezoActuator chanB;
  DiagCpu diag;
  piezoSensor sensor;
};
