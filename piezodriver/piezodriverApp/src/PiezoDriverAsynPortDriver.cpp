#include <stdlib.h>
#include <string.h>

#include "PiezoDriverAsynPortDriver.h"
#include "DiagnosticProcessor.h"

using namespace std ;


const int SEN_MAX_SAMPLES = 8192;
const int RISING_EDGE = 2;
const int SAMPLES_DROP = 4;
const int AUTO_REARM_ON = 1;
const int TIMEOUT = 1000;
const int CONT_ACQ = 1;

// Acuator params channel A
// pcd tool -p
const char *P_CHAN_A_TRIG_DELAY_RD_String = "CHAN_A_TRIG_DELAY_RD";
const char *P_CHAN_A_TRIG_OCCUR_RD_String = "CHAN_A_TRIG_OCCUR_RD";
const char *P_CHAN_A_TRIG_AUTO_REARM_RD_String = "CHAN_A_TRIG_AUTO_REARM_RD";
const char *P_CHAN_A_TRIG_STATE_RD_String = "CHAN_A_TRIG_STATE_RD";
const char *P_ACT_TRIG_LINE_RD_String = "ACT_TRIG_LINE_RD";
const char *P_ACT_CLEAR_LINE_RD_String = "ACT_CLEAR_LINE_RD";
const char *P_ACT_EVENT_NUM_RD_String = "ACT_EVENT_NUM_RD";
const char *P_ACT_TRIGGER_PER_LINE_RD_String = "ACT_TRIG_PER_LINE_RD";

// pcd tool -d
const char *P_CHAN_A_TRIG_DELAY_SP_String = "CHAN_A_TRIG_DELAY_SP";

// pcd-tool -t -a
const char *P_CHAN_A_TRIG_STATE_SP_String = "CHAN_A_TRIG_STATE_SP";
const char *P_CHAN_A_TRIG_AUTO_REARM_SP_String = "CHAN_A_TRIG_AUTO_REARM_SP";

// pcd-tool -l
const char *P_ACT_TRIGGER_LINE_SP_String = "ACT_TRIG_LINE_SP";
const char *P_ACT_CLEAR_LINE_SP_String = "ACT_CLEAR_LINE_SP";
const char *P_ACT_EVENT_NUM_SP_String = "ACT_EVENT_NUM_SP";

// Alarm parameters for channel A
const char *P_CHAN_A_ALARM_RD_String = "CHAN_A_ALARM_RD";
const char *P_CHAN_A_ALARM_OCCUR_RD_String = "CHAN_A_ALARM_OCCUR_RD";

// pcd-tool -r  protection circuit
const char *P_CHAN_A_RST_PROT_SP_String = "CHAN_A_RST_PROT_SP";
const char *P_CHAN_A_RST_PROT_RD_String = "CHAN_A_RST_PROT_RD";
const char *P_CHAN_B_RST_PROT_SP_String = "CHAN_B_RST_PROT_SP";
const char *P_CHAN_B_RST_PROT_RD_String = "CHAN_B_RST_PROT_RD";

//
const char *P_CHAN_A_WAVE_RD_String = "CHAN_A_WAVE_RD";
const char *P_CHAN_B_WAVE_RD_String = "CHAN_B_WAVE_RD";

// Actuator params channel B
// pcd tool -p
const char *P_CHAN_B_TRIG_DELAY_RD_String = "CHAN_B_TRIG_DELAY_RD";
const char *P_CHAN_B_TRIG_OCCUR_RD_String = "CHAN_B_TRIG_OCCUR_RD";
const char *P_CHAN_B_TRIG_AUTO_REARM_RD_String = "CHAN_B_TRIG_AUTO_REARM_RD";
const char *P_CHAN_B_TRIG_STATE_RD_String = "CHAN_B_TRIG_STATE_RD";

// pcd-tool -d
const char *P_CHAN_B_TRIG_DELAY_SP_String = "CHAN_B_TRIG_DELAY_SP";

// pcd -tool -t -a
const char *P_CHAN_B_TRIG_STATE_SP_String = "CHAN_B_TRIG_STATE_SP";
const char *P_CHAN_B_TRIG_AUTO_REARM_SP_String = "CHAN_B_TRIG_AUTO_REARM_SP";

// Alarm parameters for channel B
const char *P_CHAN_B_ALARM_RD_String = "CHAN_B_ALARM_RD";
const char *P_CHAN_B_ALARM_OCCUR_RD_String = "CHAN_B_ALARM_OCCUR_RD";

//Sensor param
//pcd tool -p
const char *P_SENS_PRE_TRIG_RD_String = "SENS_PRE_TRIG_RD";
const char *P_SENS_SAMPLE_COUNT_RD_String = "SENS_SAMPLE_COUNT_RD";
const char *P_SENS_DELAY_RD_String = "SENS_DELAY_RD";
const char *P_SENS_SAMPLES_TO_DROP_RD_String = "SENS_SAMPLES_TO_DROP_RD";
const char *P_SENS_TRIG_OCCUR_RD_String = "SENS_TRIG_OCCUR_RD";
const char *P_SENS_TRIG_AUTO_REARM_RD_String = "SENS_TRIG_AUTO_REARM_RD";
const char *P_SENS_TRIG_STATE_RD_String = "SENS_TRIG_STATE_RD";
const char *P_SENS_SWITCH_SP_String = "SENS_SWITCH_SP";
const char *P_SENS_CAPTURING_MODE_SP_String = "SENS_CAPTURING_MODE_SP";
const char *P_SENS_SAVING_TYPE_SP_String = "SENS_SAVING_TYPE_SP";
const char *P_SENS_TRIGGER_LINE_RD_String = "SENS_TRIG_LINE_RD";
const char *P_SENS_CLEAR_LINE_RD_String = "SENS_CLEAR_LINE_RD";
const char *P_SENS_EVENT_NUM_RD_String = "SENS_EVENT_NUM_RD";
const char *P_SENS_TRIGGER_PER_LINE_RD_String = "SENS_TRIG_PER_LINE_RD";

// Alarm parameters for sensor
const char *P_SENS_ALARM_RD_String = "SENS_ALARM_RD";
const char *P_SENS_ALARM_OCCUR_RD_String = "SENS_ALARM_OCCUR_RD";
const char *P_SENS_ALARM_GEN_RD_String = "SENS_ALARM_GEN_RD";
const char *P_SENS_ALARM_OCCUR_GEN_RD_String = "SENS_ALARM_OCCUR_GEN_RD";

// pcd tool
const char *P_TRIG_LEN_RD_String = "TRIG_LEN_RD";
const char *P_TRIG_CNT_RD_String = "TRIG_CNT_RD";

// pcd tool -D
const char *P_SENS_DELAY_SP_String = "SENS_DELAY_SP";
// pcd tool -R
const char *P_SENS_SAMPLES_TO_DROP_SP_String = "SENS_SAMPLES_TO_DROP_SP";
// pcd tool -P
const char *P_SENS_PRE_TRIG_SP_String = "SENS_PRE_TRIG_SP";
// pcd tool -T -A
const char *P_SENS_TRIG_AUTO_REARM_SP_String = "SENS_TRIG_AUTO_REARM_SP";
const char *P_SENS_TRIG_STATE_SP_String = "SENS_TRIG_STATE_SP";

// pcd tool
const char *P_SENS_SAMPLE_COUNT_SP_String = "SENS_SAMPLE_COUNT_SP";

// pcd tool -L
const char *P_SENS_TRIGGER_LINE_SP_String = "SENS_TRIG_LINE_SP";
const char *P_SENS_CLEAR_LINE_SP_String = "SENS_CLEAR_LINE_SP";
const char *P_SENS_EVENT_NUM_SP_String = "SENS_EVENT_NUM_SP";

// timeout
const char *P_SENS_TIMEOUT_SP_String = "SENS_TIMEOUT_SP";
const char *P_SENS_TIMEOUT_RD_String = "SENS_TIMEOUT_RD";

// Tirgger data
// pcd tool -2
const char *P_TRIG_PER_SEC_RD_String = "TRIG_PER_SEC_RD";
const char *P_TRIG_PER_SEC_SP_String = "TRIG_PER_SEC_SP";

// Channel switch
// pcd tool -m
const char *P_CHAN_A_SWITCH_SP_String = "CHAN_A_SWITCH_SP";
const char *P_CHAN_A_SWITCH_ACT_RD_String = "CHAN_A_SWITCH_ACT_RD";
const char *P_CHAN_A_SWITCH_DES_RD_String = "CHAN_A_SWITCH_DES_RD";

const char *P_CHAN_B_SWITCH_SP_String = "CHAN_B_SWITCH_SP";
const char *P_CHAN_B_SWITCH_ACT_RD_String = "CHAN_B_SWITCH_ACT_RD";
const char *P_CHAN_B_SWITCH_DES_RD_String = "CHAN_B_SWITCH_DES_RD";

// dma file path
const char *P_DMA_FILE_PATH_RD_String = "DMA_FILE_PATH_RD";

//device connected
const char *P_DEV_CON_RD_String = "DEV_CON_RD";

// waveform for actuators
// pcd-tool -w
const char *P_CHAN_A_WAVE_F_SP_String = "CHAN_A_WAVE_F_SP";
const char *P_CHAN_A_WAVE_F_RD_String = "CHAN_A_WAVE_F_RD";
const char *P_CHAN_A_WAVE_A_SP_String = "CHAN_A_WAVE_A_SP";
const char *P_CHAN_A_WAVE_A_RD_String = "CHAN_A_WAVE_A_RD";
const char *P_CHAN_A_WAVE_TYPE_SP_String = "CHAN_A_WAVE_TYPE_SP";
const char *P_CHAN_A_WAVE_TYPE_RD_String = "CHAN_A_WAVE_TYPE_RD";
const char *P_CHAN_A_WAVE_OFFSET_SP_String = "CHAN_A_WAVE_OFFSET_SP";
const char *P_CHAN_A_WAVE_OFFSET_RD_String = "CHAN_A_WAVE_OFFSET_RD";
const char *P_CHAN_A_WAVE_TRP_TR_RD_String = "CHAN_A_WAVE_TRP_TR_RD";
const char *P_CHAN_A_WAVE_TRP_TR_SP_String = "CHAN_A_WAVE_TRP_TR_SP";
const char *P_CHAN_A_WAVE_TRP_TF_RD_String = "CHAN_A_WAVE_TRP_TF_RD";
const char *P_CHAN_A_WAVE_TRP_TF_SP_String = "CHAN_A_WAVE_TRP_TF_SP";
const char *P_CHAN_A_WAVE_ARRAY_RD_String = "CHAN_A_WAVE_ARRAY_RD";
const char *P_CHAN_A_WAVE_ARRAY_X_RD_String = "CHAN_A_WAVE_ARRAY_X_RD";
const char *P_CHAN_A_WAVE_SWITCH_SP_String = "CHAN_A_WAVE_SWITCH_SP";
const char *P_CHAN_A_WAVE_FILE_RD_String = "CHAN_A_WAVE_FILE_RD";
const char *P_CHAN_A_WAVE_PREVIEW_TYPE_SP_String = "CHAN_A_WAVE_PREVIEW_TYPE_SP";
const char *P_CHAN_A_WAVE_PREVIEW_TYPE_RD_String = "CHAN_A_WAVE_PREVIEW_TYPE_RD";
const char *P_CHAN_A_WAVE_FILE_SWITCH_SP_String = "CHAN_A_WAVE_FILE_SWITCH_SP";
const char *P_CHAN_A_WAVE_LOAD_PD_SP_String = "CHAN_A_WAVE_LOAD_PD_SP";
const char *P_CHAN_A_WAVE_LOAD_PD_RD_String = "CHAN_A_WAVE_LOAD_PD_RD";
const char *P_CHAN_A_WAVE_LOAD_SAMPLES_SUGG_RD_String = "CHAN_A_WAVE_LOAD_SAMPLES_SUGG";
const char *P_CHAN_A_REFRESH_RD_String = "CHAN_A_REFRESH_RD";

const char *P_CHAN_B_WAVE_F_SP_String = "CHAN_B_WAVE_F_SP";
const char *P_CHAN_B_WAVE_F_RD_String = "CHAN_B_WAVE_F_RD";
const char *P_CHAN_B_WAVE_A_SP_String = "CHAN_B_WAVE_A_SP";
const char *P_CHAN_B_WAVE_A_RD_String = "CHAN_B_WAVE_A_RD";
const char *P_CHAN_B_WAVE_TYPE_SP_String = "CHAN_B_WAVE_TYPE_SP";
const char *P_CHAN_B_WAVE_TYPE_RD_String = "CHAN_B_WAVE_TYPE_RD";
const char *P_CHAN_B_WAVE_OFFSET_SP_String = "CHAN_B_WAVE_OFFSET_SP";
const char *P_CHAN_B_WAVE_OFFSET_RD_String = "CHAN_B_WAVE_OFFSET_RD";
const char *P_CHAN_B_WAVE_TRP_TR_RD_String = "CHAN_B_WAVE_TRP_TR_RD";
const char *P_CHAN_B_WAVE_TRP_TR_SP_String = "CHAN_B_WAVE_TRP_TR_SP";
const char *P_CHAN_B_WAVE_TRP_TF_RD_String = "CHAN_B_WAVE_TRP_TF_RD";
const char *P_CHAN_B_WAVE_TRP_TF_SP_String = "CHAN_B_WAVE_TRP_TF_SP";
const char *P_CHAN_B_WAVE_ARRAY_RD_String = "CHAN_B_WAVE_ARRAY_RD";
const char *P_CHAN_B_WAVE_ARRAY_X_RD_String = "CHAN_B_WAVE_ARRAY_X_RD";
const char *P_CHAN_B_WAVE_SWITCH_SP_String = "CHAN_B_WAVE_SWITCH_SP";
const char *P_CHAN_B_WAVE_FILE_RD_String = "CHAN_B_WAVE_FILE_RD";
const char *P_CHAN_B_WAVE_PREVIEW_TYPE_SP_String = "CHAN_B_WAVE_PREVIEW_TYPE_SP";
const char *P_CHAN_B_WAVE_PREVIEW_TYPE_RD_String = "CHAN_B_WAVE_PREVIEW_TYPE_RD";
const char *P_CHAN_B_WAVE_FILE_SWITCH_SP_String = "CHAN_B_WAVE_FILE_SWITCH_SP";
const char *P_CHAN_B_WAVE_LOAD_PD_SP_String = "CHAN_B_WAVE_LOAD_PD_SP";
const char *P_CHAN_B_WAVE_LOAD_PD_RD_String = "CHAN_B_WAVE_LOAD_PD_RD";
const char *P_CHAN_B_WAVE_LOAD_SAMPLES_SUGG_RD_String = "CHAN_B_WAVE_LOAD_SAMPLES_SUGG";
const char *P_CHAN_B_REFRESH_RD_String = "CHAN_B_REFRESH_RD";

// waveforms
//pcd-tool -G, -C, -N
const char *P_WAVE_CH_A_RD_String = "WAVE_CH_A_RD";
const char *P_WAVE_CH_B_RD_String = "WAVE_CH_B_RD";
const char *P_WAVE_CH_AB_RD_String = "WAVE_CH_AB_RD";
const char *P_WAVE_CH_C_RD_String = "WAVE_CH_C_RD";
const char *P_WAVE_CH_D_RD_String = "WAVE_CH_D_RD";
const char *P_WAVE_CH_CD_RD_String = "WAVE_CH_CD_RD";

const char *P_WAVE_BUFF_SP_String = "WAVE_BUFF_SP";

// diagnostic processor
// pcd-tool -M -s

const char *P_CHAN_A_DIAG_CPU_MODE_RD_String = "CHAN_A_DIAG_CPU_MODE_RD";
const char *P_CHAN_A_DIAG_CPU_VOLT_AC_RD_String = "CHAN_A_DIAG_CPU_VOLT_AC_RD";
const char *P_CHAN_A_DIAG_CPU_VOLT_DC_RD_String = "CHAN_A_DIAG_CPU_VOLT_DC_RD";
const char *P_CHAN_A_DIAG_CPU_VOLT_RMS_RD_String = "CHAN_A_DIAG_CPU_VOLT_RMS_RD";
const char *P_CHAN_A_DIAG_CPU_CUR_AC_RD_String = "CHAN_A_DIAG_CPU_CUR_AC_RD";
const char *P_CHAN_A_DIAG_CPU_CUR_DC_RD_String = "CHAN_A_DIAG_CPU_CUR_DC_RD";
const char *P_CHAN_A_DIAG_CPU_CUR_RMS_RD_String = "CHAN_A_DIAG_CPU_CUR_RMS_RD";
const char *P_CHAN_A_DIAG_CPU_STATUS_RD_String = "CHAN_A_DIAG_CPU_STATUS_RD";
const char *P_CHAN_A_DIAG_CPU_STATUS_DETAILED_RD_String = "CHAN_A_DIAG_CPU_STATUS_DETAILED_RD";
const char *P_CHAN_A_DIAG_CPU_POW_AMP_RD_String = "CHAN_A_DIAG_CPU_POW_AMP_STATUS_RD";
const char *P_CHAN_A_DIAG_CPU_POW_AMP_DETAILED_RD_String = "CHAN_A_DIAG_CPU_POW_AMP_STATUS_DETAILED_RD";
const char *P_CHAN_A_DIAG_CPU_RESISTOR_RD_String = "CHAN_A_DIAG_CPU_RESISTOR_RD";
const char *P_CHAN_A_DIAG_CPU_TEMP_RD_String = "CHAN_A_DIAG_CPU_TEMP_RD";
const char *P_CHAN_B_DIAG_CPU_MODE_RD_String = "CHAN_B_DIAG_CPU_MODE_RD";
const char *P_CHAN_B_DIAG_CPU_VOLT_AC_RD_String = "CHAN_B_DIAG_CPU_VOLT_AC_RD";
const char *P_CHAN_B_DIAG_CPU_VOLT_DC_RD_String = "CHAN_B_DIAG_CPU_VOLT_DC_RD";
const char *P_CHAN_B_DIAG_CPU_VOLT_RMS_RD_String = "CHAN_B_DIAG_CPU_VOLT_RMS_RD";
const char *P_CHAN_B_DIAG_CPU_CUR_AC_RD_String = "CHAN_B_DIAG_CPU_CUR_AC_RD";
const char *P_CHAN_B_DIAG_CPU_CUR_DC_RD_String = "CHAN_B_DIAG_CPU_CUR_DC_RD";
const char *P_CHAN_B_DIAG_CPU_CUR_RMS_RD_String = "CHAN_B_DIAG_CPU_CUR_RMS_RD";
const char *P_CHAN_B_DIAG_CPU_STATUS_RD_String = "CHAN_B_DIAG_CPU_STATUS_RD";
const char *P_CHAN_B_DIAG_CPU_STATUS_DETAILED_RD_String = "CHAN_B_DIAG_CPU_STATUS_DETAILED_RD";
const char *P_CHAN_B_DIAG_CPU_POW_AMP_RD_String = "CHAN_B_DIAG_CPU_POW_AMP_STATUS_RD";
const char *P_CHAN_B_DIAG_CPU_POW_AMP_DETAILED_RD_String = "CHAN_B_DIAG_CPU_POW_AMP_STATUS_DETAILED_RD";
const char *P_CHAN_B_DIAG_CPU_RESISTOR_RD_String = "CHAN_B_DIAG_CPU_RESISTOR_RD";
const char *P_CHAN_B_DIAG_CPU_TEMP_RD_String = "CHAN_B_DIAG_CPU_TEMP_RD";


static const char *driverName ="PiezoDriverAsynPortDriver";
void PiezoDriverIrq(void *drvPvt);
void PiezoDriverDiag(void *drvPvt);
void PiezoDriverMonitor(void *drvPvt);

PiezoDriverAsynPortDriver::PiezoDriverAsynPortDriver(const char *portName,  int maxPoints, const char *devName)
  :asynPortDriver(portName,
                  1,
                  asynInt32Mask | asynFloat64Mask | asynOctetMask | asynInt32ArrayMask | asynFloat32ArrayMask | asynDrvUserMask,
                  asynInt32Mask | asynFloat64Mask | asynOctetMask | asynInt32ArrayMask | asynFloat32ArrayMask,
                  0,
                  1,
                  0,
                  0)
{
  asynStatus status;
  const char *functionName = "PiezoDriverAsynPortDriver";

  // Actuators parameters read for channel A
  // pcd tool -p
  createParam(P_CHAN_A_TRIG_DELAY_RD_String, asynParamFloat64, &P_CHAN_A.TRIG_DELAY_RD);
  createParam(P_CHAN_A_TRIG_OCCUR_RD_String, asynParamInt32, &P_CHAN_A.TRIG_OCCUR_RD);
  createParam(P_CHAN_A_TRIG_AUTO_REARM_RD_String, asynParamInt32, &P_CHAN_A.TRIG_AUTO_REARM_RD);
  createParam(P_CHAN_A_TRIG_STATE_RD_String, asynParamInt32, &P_CHAN_A.TRIG_STATE_RD);
  createParam(P_ACT_TRIG_LINE_RD_String, asynParamInt32, &P_ACT_TRIGGER_LINE_RD);
  createParam(P_ACT_CLEAR_LINE_RD_String, asynParamInt32, &P_ACT_CLEAR_LINE_RD);
  createParam(P_ACT_EVENT_NUM_RD_String, asynParamInt32, &P_ACT_EVENT_NUM_RD);
  createParam(P_ACT_TRIGGER_PER_LINE_RD_String, asynParamInt32, &P_ACT_TRIGGER_PER_LINE_RD);


  // Alarm parameters for channel A
  createParam(P_CHAN_A_ALARM_RD_String, asynParamInt32, &P_CHAN_A.ALARM_RD);
  createParam(P_CHAN_A_ALARM_OCCUR_RD_String, asynParamInt32, &P_CHAN_A.ALARM_OCCUR_RD);

  //  Actuators paramteres read for channel B
  //  pcd tool -p
  createParam(P_CHAN_B_TRIG_DELAY_RD_String, asynParamFloat64, &P_CHAN_B.TRIG_DELAY_RD);
  createParam(P_CHAN_B_TRIG_OCCUR_RD_String, asynParamInt32, &P_CHAN_B.TRIG_OCCUR_RD);
  createParam(P_CHAN_B_TRIG_AUTO_REARM_RD_String, asynParamInt32, &P_CHAN_B.TRIG_AUTO_REARM_RD);
  createParam(P_CHAN_B_TRIG_STATE_RD_String, asynParamInt32, &P_CHAN_B.TRIG_STATE_RD);

  // pcd-tool -r
  createParam(P_CHAN_A_RST_PROT_SP_String, asynParamInt32, &P_CHAN_A.RST_PROT_SP);
  createParam(P_CHAN_A_RST_PROT_RD_String, asynParamInt32, &P_CHAN_A.RST_PROT_RD);
  createParam(P_CHAN_B_RST_PROT_SP_String, asynParamInt32, &P_CHAN_B.RST_PROT_SP);
  createParam(P_CHAN_B_RST_PROT_RD_String, asynParamInt32, &P_CHAN_B.RST_PROT_RD);

  // Alarm parameters for channel B
  createParam(P_CHAN_B_ALARM_RD_String, asynParamInt32, &P_CHAN_B.ALARM_RD);
  createParam(P_CHAN_B_ALARM_OCCUR_RD_String, asynParamInt32, &P_CHAN_B.ALARM_OCCUR_RD);

  // Alarm parameters initialization
  setIntegerParam(P_CHAN_A.ALARM_RD, 0);
  setIntegerParam(P_CHAN_A.ALARM_OCCUR_RD, 0);
  setIntegerParam(P_CHAN_B.ALARM_RD, 0);
  setIntegerParam(P_CHAN_B.ALARM_OCCUR_RD, 0);

  // Actuator parameters write for both channels
  // pcd-tool -d
  createParam(P_CHAN_A_TRIG_DELAY_SP_String, asynParamFloat64, &P_CHAN_A.TRIG_DELAY_SP);
  createParam(P_CHAN_B_TRIG_DELAY_SP_String, asynParamFloat64, &P_CHAN_B.TRIG_DELAY_SP);
  createParam(P_CHAN_A_TRIG_STATE_SP_String, asynParamInt32, &P_CHAN_A.TRIG_STATE_SP);
  createParam(P_CHAN_B_TRIG_STATE_SP_String, asynParamInt32, &P_CHAN_B.TRIG_STATE_SP);

  // pcd-tool -t -a
  createParam(P_CHAN_A_TRIG_AUTO_REARM_SP_String, asynParamInt32, &P_CHAN_A.TRIG_AUTO_REARM_SP);
  createParam(P_CHAN_B_TRIG_AUTO_REARM_SP_String, asynParamInt32, &P_CHAN_B.TRIG_AUTO_REARM_SP);

  // actuator MLVDS configuration
  // pcd-tool -l
  createParam(P_ACT_CLEAR_LINE_SP_String, asynParamInt32, &P_ACT_CLEAR_LINE_SP);
  createParam(P_ACT_TRIGGER_LINE_SP_String, asynParamInt32, &P_ACT_TRIGGER_LINE_SP);
  createParam(P_ACT_EVENT_NUM_SP_String, asynParamInt32, &P_ACT_EVENT_NUM_SP);

  // Sensor parameters read
  // pcd tool -p
  createParam(P_SENS_PRE_TRIG_RD_String, asynParamInt32, &P_SENS_PRE_TRIG_RD);
  createParam(P_SENS_SAMPLE_COUNT_RD_String, asynParamInt32, &P_SENS_SAMPLE_COUNT_RD);
  createParam(P_SENS_DELAY_RD_String, asynParamFloat64, &P_SENS_DELAY_RD);
  createParam(P_SENS_SAMPLES_TO_DROP_RD_String, asynParamInt32, &P_SENS_SAMPLES_TO_DROP_RD);
  createParam(P_SENS_TRIG_OCCUR_RD_String, asynParamInt32, &P_SENS_TRIG_OCCUR_RD);
  createParam(P_SENS_TRIG_AUTO_REARM_RD_String, asynParamInt32, &P_SENS_TRIG_AUTO_REARM_RD);
  createParam(P_SENS_TRIG_STATE_RD_String, asynParamInt32, &P_SENS_TRIG_STATE_RD);
  createParam(P_TRIG_LEN_RD_String, asynParamFloat64, &P_TRIG_LEN_RD);
  createParam(P_TRIG_CNT_RD_String, asynParamInt32, &P_TRIG_CNT_RD);
  createParam(P_SENS_TRIGGER_LINE_RD_String, asynParamInt32, &P_SENS_TRIGGER_LINE_RD);
  createParam(P_SENS_CLEAR_LINE_RD_String, asynParamInt32, &P_SENS_CLEAR_LINE_RD);
  createParam(P_SENS_EVENT_NUM_RD_String, asynParamInt32, &P_SENS_EVENT_NUM_RD);
  createParam(P_SENS_TRIGGER_PER_LINE_RD_String, asynParamInt32, &P_SENS_TRIGGER_PER_LINE_RD);

  //Sensor paramters write
  // pcd tool -D -R -S -T -A
  createParam(P_SENS_DELAY_SP_String, asynParamFloat64, &P_SENS_DELAY_SP);
  createParam(P_SENS_SAMPLES_TO_DROP_SP_String, asynParamInt32, &P_SENS_SAMPLES_TO_DROP_SP);
  createParam(P_SENS_PRE_TRIG_SP_String, asynParamInt32, &P_SENS_PRE_TRIG_SP);
  createParam(P_SENS_TRIG_AUTO_REARM_SP_String, asynParamInt32, &P_SENS_TRIG_AUTO_REARM_SP);
  createParam(P_SENS_TRIG_STATE_SP_String, asynParamInt32, &P_SENS_TRIG_STATE_SP);
  createParam(P_SENS_SAMPLE_COUNT_SP_String, asynParamInt32, &P_SENS_SAMPLE_COUNT_SP);
  createParam(P_SENS_SWITCH_SP_String, asynParamInt32, &P_SENS_SWITCH_SP);
  createParam(P_SENS_CAPTURING_MODE_SP_String, asynParamInt32, &P_SENS_CAPTURING_MODE_SP);
  createParam(P_SENS_SAVING_TYPE_SP_String, asynParamInt32, &P_SENS_SAVING_TYPE_SP);

  // sensor MLVDS configuration
  // pcd-tool -L
  createParam(P_SENS_TRIGGER_LINE_SP_String, asynParamInt32, &P_SENS_TRIGGER_LINE_SP);
  createParam(P_SENS_CLEAR_LINE_SP_String, asynParamInt32, &P_SENS_CLEAR_LINE_SP);
  createParam(P_SENS_EVENT_NUM_SP_String, asynParamInt32, &P_SENS_EVENT_NUM_SP);

  // Alarm parameters for sensor
  createParam(P_SENS_ALARM_RD_String, asynParamInt32, &P_SENS_ALARM_RD);
  createParam(P_SENS_ALARM_OCCUR_RD_String, asynParamInt32, &P_SENS_ALARM_OCCUR_RD);
  createParam(P_SENS_ALARM_GEN_RD_String, asynParamInt32, &P_SENS_ALARM_GEN_RD);
  createParam(P_SENS_ALARM_OCCUR_GEN_RD_String, asynParamInt32, &P_SENS_ALARM_OCCUR_GEN_RD);
  setIntegerParam(P_SENS_ALARM_RD, 0);
  setIntegerParam(P_SENS_ALARM_OCCUR_RD, 0);
  setIntegerParam(P_SENS_ALARM_GEN_RD, 0);
  setIntegerParam(P_SENS_ALARM_OCCUR_GEN_RD, 0);

  // Parameters initialization
  setDoubleParam(P_TRIG_LEN_RD, 0);
  setIntegerParam(P_TRIG_CNT_RD, 0);
  setIntegerParam(P_SENS_SAMPLE_COUNT_SP, SEN_MAX_SAMPLES);

  // Debug - trigger number
  // pcd tool -2
  createParam(P_TRIG_PER_SEC_RD_String, asynParamFloat64, &P_TRIG_PER_SEC_RD);
  createParam(P_TRIG_PER_SEC_SP_String, asynParamInt32, &P_TRIG_PER_SEC_SP);

  setIntegerParam(P_TRIG_PER_SEC_SP, 1);

  // Switch channels
  // pcd tool -m
  createParam(P_CHAN_A_SWITCH_SP_String, asynParamInt32, &P_CHAN_A_SWITCH.SP);
  createParam(P_CHAN_A_SWITCH_ACT_RD_String, asynParamInt32, &P_CHAN_A_SWITCH.ACT_RD);
  createParam(P_CHAN_A_SWITCH_DES_RD_String, asynParamInt32, &P_CHAN_A_SWITCH.DES_RD);

  createParam(P_CHAN_B_SWITCH_SP_String, asynParamInt32, &P_CHAN_B_SWITCH.SP);
  createParam(P_CHAN_B_SWITCH_ACT_RD_String, asynParamInt32, &P_CHAN_B_SWITCH.ACT_RD);
  createParam(P_CHAN_B_SWITCH_DES_RD_String, asynParamInt32, &P_CHAN_B_SWITCH.DES_RD);

  //dma path file
  // pcd-tool -x
  createParam(P_DMA_FILE_PATH_RD_String, asynParamOctet, &P_DMA_FILE_PATH_RD);
  setStringParam(P_DMA_FILE_PATH_RD, devName);

  // dev connected
  createParam(P_DEV_CON_RD_String, asynParamInt32, &P_DEV_CON_RD);

  // waveform for actuators
  // pcd-tool -w
  createParam(P_CHAN_A_WAVE_F_SP_String, asynParamFloat64, &P_CHAN_A_WAVE.F_SP);
  createParam(P_CHAN_A_WAVE_F_RD_String, asynParamFloat64, &P_CHAN_A_WAVE.F_RD);
  createParam(P_CHAN_A_WAVE_A_SP_String, asynParamFloat64, &P_CHAN_A_WAVE.A_SP);
  createParam(P_CHAN_A_WAVE_A_RD_String, asynParamFloat64, &P_CHAN_A_WAVE.A_RD);
  createParam(P_CHAN_A_WAVE_TYPE_SP_String, asynParamInt32, &P_CHAN_A_WAVE.TYPE_SP);
  createParam(P_CHAN_A_WAVE_TYPE_RD_String, asynParamInt32, &P_CHAN_A_WAVE.TYPE_RD);
  createParam(P_CHAN_A_WAVE_OFFSET_SP_String, asynParamFloat64, &P_CHAN_A_WAVE.OFFSET_SP);
  createParam(P_CHAN_A_WAVE_OFFSET_RD_String, asynParamFloat64, &P_CHAN_A_WAVE.OFFSET_RD);
  createParam(P_CHAN_A_WAVE_TRP_TR_SP_String, asynParamInt32, &P_CHAN_A_WAVE.TRP_TR_SP);
  createParam(P_CHAN_A_WAVE_TRP_TR_RD_String, asynParamInt32, &P_CHAN_A_WAVE.TRP_TR_RD);
  createParam(P_CHAN_A_WAVE_TRP_TF_SP_String, asynParamInt32, &P_CHAN_A_WAVE.TRP_TF_SP);
  createParam(P_CHAN_A_WAVE_TRP_TF_RD_String, asynParamInt32, &P_CHAN_A_WAVE.TRP_TF_RD);
  createParam(P_CHAN_A_WAVE_ARRAY_RD_String, asynParamFloat32Array, &P_CHAN_A_WAVE.ARRAY_RD);
  createParam(P_CHAN_A_WAVE_ARRAY_X_RD_String, asynParamInt32Array, &P_CHAN_A_WAVE.ARRAY_X_RD);
  createParam(P_CHAN_A_WAVE_SWITCH_SP_String, asynParamInt32, &P_CHAN_A_WAVE.SWITCH_SP);
  createParam(P_CHAN_A_WAVE_FILE_RD_String, asynParamFloat32Array, &P_CHAN_A_WAVE.FILE_RD);
  createParam(P_CHAN_A_WAVE_PREVIEW_TYPE_SP_String, asynParamInt32, &P_CHAN_A_WAVE.PREVIEW_TYPE_SP);
  createParam(P_CHAN_A_WAVE_PREVIEW_TYPE_RD_String, asynParamInt32, &P_CHAN_A_WAVE.PREVIEW_TYPE_RD);
  createParam(P_CHAN_A_WAVE_FILE_SWITCH_SP_String, asynParamInt32, &P_CHAN_A_WAVE.FILE_SWITCH_SP);
  createParam(P_CHAN_A_WAVE_LOAD_PD_SP_String, asynParamInt32, &P_CHAN_A_WAVE.LOAD_PD_SP);
  createParam(P_CHAN_A_WAVE_LOAD_PD_RD_String, asynParamInt32, &P_CHAN_A_WAVE.LOAD_PD_RD);
  createParam(P_CHAN_A_WAVE_LOAD_SAMPLES_SUGG_RD_String, asynParamInt32, &P_CHAN_A_WAVE.LOAD_SAMPLES_SUGG_RD);
  createParam(P_CHAN_A_REFRESH_RD_String, asynParamInt32, &P_CHAN_A.REFRESH_RD);

  // diagnostic processor
  // pcd-tool -M -s
  createParam(P_CHAN_A_DIAG_CPU_MODE_RD_String, asynParamOctet, &P_CHAN_A_DIAG_CPU.MODE_RD);
  createParam(P_CHAN_A_DIAG_CPU_VOLT_AC_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.VOLT_AC_RD);
  createParam(P_CHAN_A_DIAG_CPU_VOLT_DC_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.VOLT_DC_RD);
  createParam(P_CHAN_A_DIAG_CPU_VOLT_RMS_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.VOLT_RMS_RD);
  createParam(P_CHAN_A_DIAG_CPU_CUR_AC_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.CUR_AC_RD);
  createParam(P_CHAN_A_DIAG_CPU_CUR_DC_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.CUR_DC_RD);
  createParam(P_CHAN_A_DIAG_CPU_CUR_RMS_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.CUR_RMS_RD);
  createParam(P_CHAN_A_DIAG_CPU_STATUS_RD_String, asynParamOctet, &P_CHAN_A_DIAG_CPU.STATUS_RD);
  createParam(P_CHAN_A_DIAG_CPU_STATUS_DETAILED_RD_String, asynParamOctet, &P_CHAN_A_DIAG_CPU.DETAILED_STATUS_RD);
  createParam(P_CHAN_A_DIAG_CPU_POW_AMP_RD_String, asynParamOctet, &P_CHAN_A_DIAG_CPU.POW_AMP_RD);
  createParam(P_CHAN_A_DIAG_CPU_POW_AMP_DETAILED_RD_String, asynParamOctet, &P_CHAN_A_DIAG_CPU.DETAILED_POW_AMP_RD);
  createParam(P_CHAN_A_DIAG_CPU_RESISTOR_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.RESISTOR_RD);
  createParam(P_CHAN_A_DIAG_CPU_TEMP_RD_String, asynParamFloat64, &P_CHAN_A_DIAG_CPU.TEMP_RD);
  createParam(P_CHAN_B_DIAG_CPU_MODE_RD_String, asynParamOctet, &P_CHAN_B_DIAG_CPU.MODE_RD);
  createParam(P_CHAN_B_DIAG_CPU_VOLT_AC_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.VOLT_AC_RD);
  createParam(P_CHAN_B_DIAG_CPU_VOLT_DC_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.VOLT_DC_RD);
  createParam(P_CHAN_B_DIAG_CPU_VOLT_RMS_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.VOLT_RMS_RD);
  createParam(P_CHAN_B_DIAG_CPU_CUR_AC_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.CUR_AC_RD);
  createParam(P_CHAN_B_DIAG_CPU_CUR_DC_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.CUR_DC_RD);
  createParam(P_CHAN_B_DIAG_CPU_CUR_RMS_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.CUR_RMS_RD);
  createParam(P_CHAN_B_DIAG_CPU_STATUS_RD_String, asynParamOctet, &P_CHAN_B_DIAG_CPU.STATUS_RD);
  createParam(P_CHAN_B_DIAG_CPU_STATUS_DETAILED_RD_String, asynParamOctet, &P_CHAN_B_DIAG_CPU.DETAILED_STATUS_RD);
  createParam(P_CHAN_B_DIAG_CPU_POW_AMP_RD_String, asynParamOctet, &P_CHAN_B_DIAG_CPU.POW_AMP_RD);
  createParam(P_CHAN_B_DIAG_CPU_POW_AMP_DETAILED_RD_String, asynParamOctet, &P_CHAN_B_DIAG_CPU.DETAILED_POW_AMP_RD);
  createParam(P_CHAN_B_DIAG_CPU_RESISTOR_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.RESISTOR_RD);
  createParam(P_CHAN_B_DIAG_CPU_TEMP_RD_String, asynParamFloat64, &P_CHAN_B_DIAG_CPU.TEMP_RD);

  setIntegerParam(P_CHAN_A.REFRESH_RD, 0);
  setIntegerParam(P_CHAN_A_WAVE.FILE_SWITCH_SP, 0);
  setDoubleParam(P_CHAN_A_WAVE.F_SP, 1e3f);
  setDoubleParam(P_CHAN_A_WAVE.A_SP, 5.0f);
  setIntegerParam(P_CHAN_A_WAVE.TYPE_SP, 1);
  setDoubleParam(P_CHAN_A_WAVE.OFFSET_SP, 0);
  setIntegerParam(P_CHAN_A_WAVE.PREVIEW_TYPE_SP, 0);

  createParam(P_CHAN_B_WAVE_F_SP_String, asynParamFloat64, &P_CHAN_B_WAVE.F_SP);
  createParam(P_CHAN_B_WAVE_F_RD_String, asynParamFloat64, &P_CHAN_B_WAVE.F_RD);
  createParam(P_CHAN_B_WAVE_A_SP_String, asynParamFloat64, &P_CHAN_B_WAVE.A_SP);
  createParam(P_CHAN_B_WAVE_A_RD_String, asynParamFloat64, &P_CHAN_B_WAVE.A_RD);
  createParam(P_CHAN_B_WAVE_TYPE_SP_String, asynParamInt32, &P_CHAN_B_WAVE.TYPE_SP);
  createParam(P_CHAN_B_WAVE_TYPE_RD_String, asynParamInt32, &P_CHAN_B_WAVE.TYPE_RD);
  createParam(P_CHAN_B_WAVE_OFFSET_SP_String, asynParamFloat64, &P_CHAN_B_WAVE.OFFSET_SP);
  createParam(P_CHAN_B_WAVE_OFFSET_RD_String, asynParamFloat64, &P_CHAN_B_WAVE.OFFSET_RD);
  createParam(P_CHAN_B_WAVE_TRP_TR_SP_String, asynParamInt32, &P_CHAN_B_WAVE.TRP_TR_SP);
  createParam(P_CHAN_B_WAVE_TRP_TR_RD_String, asynParamInt32, &P_CHAN_B_WAVE.TRP_TR_RD);
  createParam(P_CHAN_B_WAVE_TRP_TF_SP_String, asynParamInt32, &P_CHAN_B_WAVE.TRP_TF_SP);
  createParam(P_CHAN_B_WAVE_TRP_TF_RD_String, asynParamInt32, &P_CHAN_B_WAVE.TRP_TF_RD);
  createParam(P_CHAN_B_WAVE_ARRAY_RD_String, asynParamFloat32Array, &P_CHAN_B_WAVE.ARRAY_RD);
  createParam(P_CHAN_B_WAVE_ARRAY_X_RD_String, asynParamInt32Array, &P_CHAN_B_WAVE.ARRAY_X_RD);
  createParam(P_CHAN_B_WAVE_SWITCH_SP_String, asynParamInt32, &P_CHAN_B_WAVE.SWITCH_SP);
  createParam(P_CHAN_B_WAVE_FILE_RD_String, asynParamFloat32Array, &P_CHAN_B_WAVE.FILE_RD);
  createParam(P_CHAN_B_WAVE_PREVIEW_TYPE_SP_String, asynParamInt32, &P_CHAN_B_WAVE.PREVIEW_TYPE_SP);
  createParam(P_CHAN_B_WAVE_PREVIEW_TYPE_RD_String, asynParamInt32, &P_CHAN_B_WAVE.PREVIEW_TYPE_RD);
  createParam(P_CHAN_B_WAVE_FILE_SWITCH_SP_String, asynParamInt32, &P_CHAN_B_WAVE.FILE_SWITCH_SP);
  createParam(P_CHAN_B_WAVE_LOAD_PD_SP_String, asynParamInt32, &P_CHAN_B_WAVE.LOAD_PD_SP);
  createParam(P_CHAN_B_WAVE_LOAD_PD_RD_String, asynParamInt32, &P_CHAN_B_WAVE.LOAD_PD_RD);
  createParam(P_CHAN_B_WAVE_LOAD_SAMPLES_SUGG_RD_String, asynParamInt32, &P_CHAN_B_WAVE.LOAD_SAMPLES_SUGG_RD);
  createParam(P_CHAN_B_REFRESH_RD_String, asynParamInt32, &P_CHAN_B.REFRESH_RD);

  setIntegerParam(P_CHAN_B.REFRESH_RD, 0);
  setIntegerParam(P_CHAN_B_WAVE.FILE_SWITCH_SP, 0);
  setDoubleParam(P_CHAN_B_WAVE.F_SP, 1e3f);
  setDoubleParam(P_CHAN_B_WAVE.A_SP, 5.0f);
  setIntegerParam(P_CHAN_B_WAVE.TYPE_SP, 1);
  setDoubleParam(P_CHAN_B_WAVE.OFFSET_SP, 0);
  setIntegerParam(P_CHAN_B_WAVE.PREVIEW_TYPE_SP, 0);

  // waveforms
  // pcd-tool - N -G
  createParam(P_WAVE_CH_A_RD_String, asynParamFloat32Array, &P_WAVE_CH_A_RD);
  createParam(P_WAVE_CH_B_RD_String, asynParamFloat32Array, &P_WAVE_CH_B_RD);
  createParam(P_WAVE_CH_AB_RD_String, asynParamFloat32Array, &P_WAVE_CH_AB_RD);
  createParam(P_WAVE_CH_C_RD_String, asynParamFloat32Array, &P_WAVE_CH_C_RD);
  createParam(P_WAVE_CH_D_RD_String, asynParamFloat32Array, &P_WAVE_CH_D_RD);
  createParam(P_WAVE_CH_CD_RD_String, asynParamFloat32Array, &P_WAVE_CH_CD_RD);

  createParam(P_WAVE_BUFF_SP_String, asynParamInt32, &P_WAVE_BUFF_SP);

  createParam(P_SENS_TIMEOUT_RD_String, asynParamInt32, &P_SENS_TIMEOUT_RD);
  createParam(P_SENS_TIMEOUT_SP_String, asynParamInt32, &P_SENS_TIMEOUT_SP);

  // initialize device
  asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Initializing parameters\n");

  deviceName = new char[strlen(devName) +1];
  strcpy(deviceName, devName);
  deviceConnected = initializeDevice();
  setIntegerParam(P_DEV_CON_RD, (int) deviceConnected);

  if (deviceConnected)
  {
    asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Configure actuators and sensor\n");
    configureActuators();
    configureSensor();
  }
  // Initialize acquisition
  initAcquisition();

  terminateWorkers = 0;
  terminateWorkersLock = epicsMutexMustCreate();

  ackFromWorkersCount = 0;
  pthread_mutex_init(&ackFromWorkersLock, NULL);
  pthread_cond_init(&ackFromWorkersCond, NULL);

  // IRQ thread
  status = (asynStatus)(epicsThreadCreate("PiezoDriverIrq",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)::PiezoDriverIrq,
                                          this) == NULL);

  if (status)
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_SOURCE | ASYN_TRACEINFO_TIME, "%s:%s: epicsThreadCreate failure\n", driverName, functionName);
    return;
  }

  // Diagnostic thread
  status = (asynStatus)(epicsThreadCreate("PiezoDriverDiag",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)::PiezoDriverDiag,
                                          this) == NULL);

  if (status)
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_SOURCE | ASYN_TRACEINFO_TIME, "%s:%s: epicsThreadCreate failure\n", driverName, functionName);
    return;
  }

  // Monitor thread
  status = (asynStatus)(epicsThreadCreate("PiezoDriverMonitor",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)::PiezoDriverMonitor,
                                          this) == NULL);

  if (status)
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_SOURCE | ASYN_TRACEINFO_TIME, "%s:%s: epicsThreadCreate failure\n", driverName, functionName);
    return;
  }
}

asynStatus PiezoDriverAsynPortDriver::writeFloat32Array(asynUser *pasynUser, epicsFloat32 *value,  size_t nElements)
{
  int function = pasynUser->reason;
  // copy inserted samples to internal array chan A
  if (function == P_CHAN_A_WAVE.FILE_RD)
  {
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, "Loading waveform from file to channel A\n");
    number_of_samples_a = nElements;
    lock();
    setIntegerParam(P_CHAN_A_WAVE.LOAD_SAMPLES_SUGG_RD, number_of_samples_a);
    setIntegerParam(P_CHAN_A.REFRESH_RD, 1);
    unlock();
    for (int i = 0; i < number_of_samples_a; i++)
    {
      array_a[i] = value[i];
    }
  }
  // copy inserted samples to internal array chan B
  if (function == P_CHAN_B_WAVE.FILE_RD)
  {
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, "Loading waveform from file to channel B\n");
    number_of_samples_b = nElements;
    lock();
    setIntegerParam(P_CHAN_B_WAVE.LOAD_SAMPLES_SUGG_RD, number_of_samples_b);
    setIntegerParam(P_CHAN_B.REFRESH_RD, 1);
    unlock();
    for (int i = 0; i < number_of_samples_b; i++)
    {
      array_b[i] = value[i];
    }
  }
  asynStatus status = asynSuccess;
  return status;
}

void PiezoDriverAsynPortDriver::configureActuators()
{
  chanA.configChan(CHAN_A, dev);
  chanB.configChan(CHAN_B, dev);
  diag.config(dev);

  // actuator parameters initialization
  double delayA, delayB;
  int occurA, occurB;
  int trigA, trigB, rearmA, rearmB;
  int dirA, dirB, actA, actB;
  int tripA, tripB;
  int res;
  int trigLine, clear, evt;

  res = chanA.getTrigDel(&delayA);
  actAlarmProcedure(res, CHAN_A);
  res = chanA.getTrigOccur(&occurA);
  actAlarmProcedure(res, CHAN_A);
  res = chanA.getTrigType(&trigA, &rearmA);
  actAlarmProcedure(res, CHAN_A);
  res = chanA.getDesiredChanDir(&dirA);
  actAlarmProcedure(res, CHAN_A);
  res = chanA.getActualChanDir(&actA);
  actAlarmProcedure(res, CHAN_A);
  res = chanA.getProtection(&tripA);
  actAlarmProcedure(res, CHAN_A);

  res = chanB.getTrigDel(&delayB);
  actAlarmProcedure(res, CHAN_B);
  res = chanB.getTrigOccur(&occurB);
  actAlarmProcedure(res, CHAN_B);
  res = chanB.getTrigType(&trigB, &rearmB);
  actAlarmProcedure(res, CHAN_B);
  res = chanB.getDesiredChanDir(&dirB);
  actAlarmProcedure(res, CHAN_B);
  res = chanB.getActualChanDir(&actB);
  actAlarmProcedure(res, CHAN_B);
  res = chanB.getProtection(&tripB);
  actAlarmProcedure(res, CHAN_B);

  res = chanA.getTrigSrc(&trigLine, &clear, &evt);
  actAlarmProcedure(res, CHAN_A);

  lock();
  setDoubleParam(P_CHAN_A.TRIG_DELAY_RD, delayA * 1000);
  setDoubleParam(P_CHAN_A.TRIG_DELAY_SP, delayA * 1000);
  setIntegerParam(P_CHAN_A.TRIG_OCCUR_RD, occurA);
  setIntegerParam(P_CHAN_A.RST_PROT_RD, tripA);
  setIntegerParam(P_CHAN_A_SWITCH.ACT_RD, actA);
  setIntegerParam(P_CHAN_A_SWITCH.DES_RD, dirA);
  setIntegerParam(P_CHAN_A_SWITCH.SP, actA);

  setDoubleParam(P_CHAN_B.TRIG_DELAY_RD, delayB * 1000);
  setDoubleParam(P_CHAN_B.TRIG_DELAY_SP, delayB * 1000);
  setIntegerParam(P_CHAN_B.TRIG_OCCUR_RD, occurB);
  setIntegerParam(P_CHAN_B.RST_PROT_RD, tripB);
  setIntegerParam(P_CHAN_B_SWITCH.ACT_RD, actB);
  setIntegerParam(P_CHAN_B_SWITCH.DES_RD, dirB);
  setIntegerParam(P_CHAN_B_SWITCH.SP, actB);

  setIntegerParam(P_ACT_TRIGGER_LINE_RD, trigLine);
  setIntegerParam(P_ACT_CLEAR_LINE_RD, clear);
  setIntegerParam(P_ACT_EVENT_NUM_RD, evt);
  setIntegerParam(P_ACT_TRIGGER_LINE_SP, trigLine);
  setIntegerParam(P_ACT_CLEAR_LINE_SP, clear);
  setIntegerParam(P_ACT_EVENT_NUM_SP, evt);

  unlock();

}

void PiezoDriverAsynPortDriver::configureSensor()
{
  sensor.config(dev);
  // sensor parameters initialization
  int enable, trigger, type, arm, sample_cnt, length, cnt, drop;
  int res;
  double delay;
  int trig_sen, clear_sen, evt_sen;

  res = sensor.getPreTrig(&enable);
  sensAlarmProcedure(res);
  res = sensor.getTrigDel(&delay);
  sensAlarmProcedure(res);
  res = sensor.getSampleDrop(&drop);
  sensAlarmProcedure(res);
  res = sensor.getTrigOccur(&trigger);
  sensAlarmProcedure(res);
  res = sensor.getTrigType(&type, &arm);
  sensAlarmProcedure(res);
  res = sensor.getTrigInfo(&length, &cnt);
  sensAlarmProcedure(res);
  res = sensor.getSampleCnt(&sample_cnt);
  sensAlarmProcedure(res);
  res = sensor.getTrigSrc(&trig_sen, &clear_sen, &evt_sen);
  sensAlarmProcedure(res);

  lock();
  setIntegerParam(P_SENS_PRE_TRIG_RD, enable);
  setIntegerParam(P_SENS_PRE_TRIG_SP, enable);
  setIntegerParam(P_SENS_SAMPLE_COUNT_RD, sample_cnt);
  setDoubleParam(P_SENS_DELAY_RD, delay * 1000.0);
  setDoubleParam(P_SENS_DELAY_SP, delay * 1000.0);
  setIntegerParam(P_SENS_TRIG_OCCUR_RD, trigger);
  setDoubleParam(P_TRIG_LEN_RD, length / 100.0f);
  setIntegerParam(P_TRIG_CNT_RD, cnt);
  setIntegerParam(P_SENS_TRIGGER_LINE_RD, trig_sen);
  setIntegerParam(P_SENS_CLEAR_LINE_RD, clear_sen);
  setIntegerParam(P_SENS_EVENT_NUM_RD, evt_sen);
  setIntegerParam(P_SENS_TRIGGER_LINE_SP, trig_sen);
  setIntegerParam(P_SENS_CLEAR_LINE_SP, clear_sen);
  setIntegerParam(P_SENS_EVENT_NUM_SP, evt_sen);

  unlock();
}

bool PiezoDriverAsynPortDriver::initializeDevice()
{
  dev = xil_open_device(deviceName);

  if (!dev)
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "Can't connect to device\n");
    return false;
  }
  return true;
}

asynStatus PiezoDriverAsynPortDriver::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  status = (asynStatus) setIntegerParam(function, value);

  // change of sensor parameters
  if ((function == P_SENS_SAMPLES_TO_DROP_SP) || (function == P_SENS_SAMPLE_COUNT_SP) || (function == P_SENS_PRE_TRIG_SP) || (function == P_SENS_TRIG_AUTO_REARM_SP) || (function == P_SENS_TRIG_STATE_SP) || (function == P_SENS_TIMEOUT_SP))
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Sensor parameters change\n");
    P_SENS_CONFIG_needs_update = true;
  }

  // starting calibrating trigger procedure
  if (function == P_TRIG_PER_SEC_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Calibration trigger procedure start\n");
    int trig_activation;
    getIntegerParam(P_TRIG_PER_SEC_SP, &trig_activation);
    if (trig_activation == 1)
    {
      time_start = true;
    }
  }

  // switch channel A mode
  if (function == P_CHAN_A_SWITCH.SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Channel A mode switch\n");
    P_CHAN_A_SWITCH_needs_update = true;
  }

  // switch channel B mode
  if (function == P_CHAN_B_SWITCH.SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Channel B mode switch\n");
    P_CHAN_B_SWITCH_needs_update = true;
  }

  // change of channel A and B driver paramters trigger state and autorearm mode
  if ((function == P_CHAN_A.TRIG_AUTO_REARM_SP) || (function == P_CHAN_B.TRIG_AUTO_REARM_SP) || (function == P_CHAN_A.TRIG_STATE_SP) || function == (P_CHAN_B.TRIG_STATE_SP))
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Actuator trigger parameter change\n");
    P_ACTUATORS_needs_update = true;
  }

  // catch registered waveforms
  if (function == P_SENS_SWITCH_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Sensor data acquisition\n");
    P_WAVEFORM_needs_update = true;
  }

  // change of capturing mode (single/continuous)
  if (function == P_SENS_CAPTURING_MODE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Capturing mode change\n");
    P_SENS_CAPTURING_MODE_needs_update = true;
  }

  // load samples from channel A to piezo driver
  if (function == P_CHAN_A_WAVE.SWITCH_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Loading channel A samples to driver\n");
    P_CHAN_A_WAVE_needs_update = true;
  }

  // load samples from channel B to piezo driver
  if (function == P_CHAN_B_WAVE.SWITCH_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Loading channel B samples to driver\n");
    P_CHAN_B_WAVE_needs_update = true;
  }

  // change of waveform type to be sent to piezo driver chan A
  if (function == P_CHAN_A_WAVE.TYPE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing channel A waveform type\n");
    P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  // change of waveform type to be sent to piezo driver chan B
  if (function == P_CHAN_B_WAVE.TYPE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing channel B waveform type\n");
    P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  // change trapeze parameters used to generate waveform chan A
  if (function == P_CHAN_A_WAVE.TRP_TR_SP || function == P_CHAN_A_WAVE.TRP_TF_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing trapeze parameters channel A\n");
    P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  // change trapeze parameters used to generate waveform chan B
  if (function == P_CHAN_B_WAVE.TRP_TR_SP || function == P_CHAN_B_WAVE.TRP_TF_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing trapeze parameters channel B\n");
    P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  // loading samples from file to piezo driver chan A
  if (function == P_CHAN_A_WAVE.LOAD_PD_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Loading waveform from file channel A\n");
    P_CHAN_A_WAVE_LOAD_PD_needs_update = true;
  }

  // loading samples from file to piezo driver chan B
  if (function == P_CHAN_B_WAVE.LOAD_PD_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Loading waveform from file channel B\n");
    P_CHAN_B_WAVE_LOAD_PD_needs_update = true;
  }

  // reset protection circuit channel A
  if (function == P_CHAN_A.RST_PROT_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Resetting protection circuit channel A\n");
    P_CHAN_A_RST_PROT_needs_update = true;
  }

  // reset protection circuit channel B
  if (function == P_CHAN_B.RST_PROT_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Resetting protection circuit channel B\n");
    P_CHAN_B_RST_PROT_needs_update = true;
  }

  // change trigger line for actuator channels
  if (function == P_ACT_TRIGGER_LINE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting trigger line for actuators\n");
    P_ACTUATORS_needs_update = true;
  }

  // change clear line for actuator channels
  if (function == P_ACT_CLEAR_LINE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting trigger line actuators\n");
    P_ACTUATORS_needs_update = true;
  }

  // set event number for actuators
  if (function == P_ACT_EVENT_NUM_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting event number for actuators\n");
    P_ACTUATORS_needs_update = true;
  }

  // change trigger line for sensor channel
  if (function == P_SENS_TRIGGER_LINE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting trigger line for sensor\n");
    P_SENS_CONFIG_needs_update = true;
  }

  // change clear line for sensor channel
  if (function == P_SENS_CLEAR_LINE_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting trigger line sensor\n");
    P_SENS_CONFIG_needs_update = true;
  }

  // set event number for sensor channel
  if (function == P_SENS_EVENT_NUM_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Setting event number sensor\n");
    P_SENS_CONFIG_needs_update = true;
  }

  return status;
}

asynStatus PiezoDriverAsynPortDriver::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  status = (asynStatus) setDoubleParam(function, value);

  // change of delay trigger sensor
  if (function == P_SENS_DELAY_SP)
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing sensor delay\n");
    P_SENS_CONFIG_needs_update = true;
  }

  // change of delay trigger for both channels
  if ((function == P_CHAN_A.TRIG_DELAY_SP) || (function == P_CHAN_B.TRIG_DELAY_SP))
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Changing actuator trigger delay\n");
    P_ACTUATORS_needs_update = true;
  }

  // change of waveform parameters for channel A
  if ((function == P_CHAN_A_WAVE.F_SP) || (function == P_CHAN_A_WAVE.A_SP) || (function == P_CHAN_A_WAVE.OFFSET_SP))
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Chaning waveform parameters channel A\n");
    P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  // change of waveform parameters for channel B
  if ((function == P_CHAN_B_WAVE.F_SP) || (function == P_CHAN_B_WAVE.A_SP) || (function == P_CHAN_B_WAVE.OFFSET_SP))
  {
    asynPrint(pasynUser, ASYN_TRACE_FLOW, "Chaning waveform parameters channel B\n");
    P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update = true;
  }

  return status;
}

void PiezoDriverAsynPortDriver::actAlarmProcedure(int result, int channel)
{
  lock();
  if (channel == CHAN_A)
  {
    setIntegerParam(P_CHAN_A.ALARM_RD, abs(result));
    if (result == 0)
    {
      setIntegerParam(P_CHAN_A.ALARM_OCCUR_RD, 0);
    }
    else
    {
      asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_TIME | ASYN_TRACEINFO_SOURCE, "%s\n", pcd_error_string(result));
      setIntegerParam(P_CHAN_A.ALARM_OCCUR_RD, 1);
    }
  }
  else if (channel == CHAN_B)
  {
    setIntegerParam(P_CHAN_B.ALARM_RD, abs(result));
    if (result == 0)
    {
      setIntegerParam(P_CHAN_B.ALARM_OCCUR_RD, 0);
    }
    else
    {
      setIntegerParam(P_CHAN_B.ALARM_OCCUR_RD, 1);
      asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_TIME | ASYN_TRACEINFO_SOURCE, "%s\n", pcd_error_string(result));
    }
  }
  unlock();
}

void PiezoDriverAsynPortDriver::setLimits(int down, int up, int* var)
{
  if (*var < down)
  {
    *var = down;
  }
  else if (*var > up)
  {
    *var = up;
  }
}

void PiezoDriverAsynPortDriver::setLimits(int down, int up, double* var)
{
  if (*var < down)
  {
    *var = down;
  }
  else if (*var > up)
  {
    *var = up;
  }
}

void PiezoDriverAsynPortDriver::sensAlarmProcedure(int result)
{
  lock();
  setIntegerParam(P_SENS_ALARM_GEN_RD, abs(result));
  if (result == 0)
  {
    setIntegerParam(P_SENS_ALARM_OCCUR_GEN_RD, 0);
  }
  else
  {
    asynPrint(pasynUserSelf, ASYN_TRACE_ERROR | ASYN_TRACEINFO_TIME | ASYN_TRACEINFO_SOURCE, "%s\n", pcd_error_string(result));
    setIntegerParam(P_SENS_ALARM_OCCUR_GEN_RD, 1);
  }
  unlock();
}

void PiezoDriverAsynPortDriver::initAcquisition()
{
  asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "Initialization of acquisition\n");
  lock();
  // set rising edge trigger - actuator and sensor
  setIntegerParam(P_CHAN_A.TRIG_STATE_SP, RISING_EDGE);
  setIntegerParam(P_CHAN_B.TRIG_STATE_SP, RISING_EDGE);
  setIntegerParam(P_SENS_TRIG_STATE_SP, RISING_EDGE);
  // set auto rearm on
  setIntegerParam(P_CHAN_A.TRIG_AUTO_REARM_SP, AUTO_REARM_ON);
  setIntegerParam(P_CHAN_B.TRIG_AUTO_REARM_SP, AUTO_REARM_ON);
  setIntegerParam(P_SENS_TRIG_AUTO_REARM_SP, AUTO_REARM_ON);
  // turn on continuous acqusition
  setIntegerParam(P_SENS_CAPTURING_MODE_SP, CONT_ACQ);
  // number of samples for acquisition
  setIntegerParam(P_SENS_SAMPLES_TO_DROP_SP, SAMPLES_DROP);
  setIntegerParam(P_WAVE_BUFF_SP, SEN_MAX_SAMPLES);
  // timeout
  setIntegerParam(P_SENS_TIMEOUT_SP, TIMEOUT);
  unlock();
}

void PiezoDriverAsynPortDriver::TerminateAllThreads()
{
  epicsMutexMustLock(terminateWorkersLock);
  terminateWorkers = 1 ;
  epicsMutexUnlock(terminateWorkersLock);
  pthread_mutex_lock(&ackFromWorkersLock);
  do
  {
    if (ackFromWorkersCount == 3)
    {
      break;
    }
    else
    {
      pthread_cond_wait(&ackFromWorkersCond, &ackFromWorkersLock);
    }
  }
  while (1) ;

  pthread_mutex_unlock(&ackFromWorkersLock);
}

PiezoDriverAsynPortDriver::~PiezoDriverAsynPortDriver()
{
  asynPrint(pasynUserSelf, ASYN_TRACEIO_DEVICE, "Desctructor called\n");
  // close device
  xil_close_device(dev);
}
