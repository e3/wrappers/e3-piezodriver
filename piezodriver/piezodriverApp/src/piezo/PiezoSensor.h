#ifndef _PIEZO_SENS_H_
#define _PIEZO_SENS_H_

#include "libpcd.h"
#include <epicsTypes.h>


class piezoSensor
{
public:
  void config(xildev *dev);
  int getPreTrig(int *if_enabled);
  int getSampleCnt(int *samples);
  int getTrigDel(double *delay);
  int getSampleDrop(int *samples);
  int getTrigOccur(int *occurred);
  int getTrigType(int *type, int *rearm);
  int getTrigInfo(int *length, int *cnt);
  int getTrigSrc(int *trig_line, int *stop_line, int *evt_no);
  int getTrigPerLine(int *trig_count);


  int setTrigDel(double delay);
  int setSampleDrop(int samples);
  int setSampleCnt(int samples);
  int setPreTrig(int state);
  int setTrigArm(int type, int rearm);
  int setTrigSrc(int trig_line, int clear_line, int event_num);

  int getSensData(int samples, int timeout, int chA, int chB, epicsFloat32** pvData, int trig_type, int rearm);
protected:

private:
  xildev *device;

  PcdTriggerType valueToEnum(int enum_int);
  PcdTriggerLine intToLine(int line);
  int pcd_sen_read_data_with_trigger(PcdSensorSample *storage, uint32_t samples, unsigned int timeout, int trig_type, int rearm);
};
#endif
