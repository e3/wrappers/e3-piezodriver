#include <iostream>
#include <unistd.h>

#include "PiezoSensor.h"


const int MIN_ARRAY_LENGTH = 128;
const int MAX_ARRAY_LENGTH = 8192;
const int ADC_BITS = 16;
const double ADC_REF_VOLT = (4.096 * 2);
const double CURR_OFFSET = 1.67;
const double CURR_SENS = 0.066;
const int VOLT_DIV = 61;
const int NUM_LINES = 8;


void piezoSensor::config(xildev *dev)
{
  device = dev;
}

// read pre tigger state
int piezoSensor::getPreTrig(int *if_enabled)
{
  int res;
  bool state;
  res = pcd_sen_get_pre_trigger(device, &state);
  *if_enabled = state?1:0;
  return res;
}

// read number of samples for acquisition
int piezoSensor::getSampleCnt(int *samples)
{
  int res;
  uint16_t samps;
  res = pcd_sen_get_sample_count(device, &samps);
  *samples = (int)samps;
  return res;
}

// read sensor trigger delay
int piezoSensor::getTrigDel(double *delay)
{
  int res;
  res = pcd_sen_get_trigger_delay(device, delay);
  return res;
}

// read number of samples to drop
int piezoSensor::getSampleDrop(int *samples)
{
  int res;
  uint8_t drops;
  res = pcd_sen_get_sample_dropping(device, &drops);
  *samples = (int)drops;
  return res;
}

// check if trigger occurred
int piezoSensor::getTrigOccur(int *occurred)
{
  int res;
  bool triggered;
  res = pcd_sen_was_triggered(device, &triggered);
  *occurred = triggered?1:0;
  return res;
}

// read sensor trigger type and auto-rearm status
int piezoSensor::getTrigType(int *type, int *rearm)
{
  int res;
  bool auto_rearm_sens;
  enum PcdTriggerType trig_enum_sens;
  res = pcd_sen_get_trigger_type(device, &trig_enum_sens, &auto_rearm_sens);
  *type = (int)trig_enum_sens;
  *rearm = auto_rearm_sens?1:0;
  return res;
}

// read MLVDS configuration
int piezoSensor::getTrigSrc(int *trig_line, int *stop_line, int *evt_no)
{
  int res;
  enum PcdTriggerLine trig_line_enum, stop_line_enum;
  uint8_t evt;

  //res  = pcd_act_get_trigger_source(device, &trig_line_enum, &stop_line_enum, evt_no);
  res  = pcd_sen_get_trigger_source(device, &trig_line_enum, &stop_line_enum, &evt);
  *trig_line = (int)trig_line_enum;
  *stop_line = (int)stop_line_enum;
  *evt_no = int(evt);

  return res;
}

// read number of triggers per trigger line
int piezoSensor::getTrigPerLine(int *trig_count)
{
  int res;
  uint8_t trig;
  res = pcd_sen_get_triggers_per_line(device, &trig);
  *trig_count = (int)trig;
  return res;
}

// read information about trigger - its length and number of occurance
int piezoSensor::getTrigInfo(int *length, int *cnt)
{
  int res;
  uint8_t len, count;
  res = pcd_sen_get_trigger_info(device, &len, &count);
  *length = (int)len;
  *cnt = (int)count;
  return res;
}

// set sensor trigger delay
int piezoSensor::setTrigDel(double delay)
{
  int res;
  res = pcd_sen_set_trigger_delay(device, delay);
  return res;
}

// set number of samples to drop
int piezoSensor::setSampleDrop(int samples)
{
  int res;
  res = pcd_sen_set_sample_dropping(device, samples);
  return res;
}

// set number of samples for acquisition
int piezoSensor::setSampleCnt(int samples)
{
  int res;
  res = pcd_sen_set_sample_count(device, samples);
  return res;
}

// set pre trigger state
int piezoSensor::setPreTrig(int state)
{
  int res;
  bool pre_trig = state? true:false;
  res = pcd_sen_set_pre_trigger(device, pre_trig);
  return res;
}

// set trigger type and auto-rearm
int piezoSensor::setTrigArm(int type, int rearm)
{
  int res;
  bool auto_rearm = rearm?1:0;
  enum PcdTriggerType trig_type;
  trig_type = valueToEnum(type);
  res = pcd_sen_trigger_arm(device, trig_type, auto_rearm);
  return res;
}

// configure M-LVDS
int piezoSensor::setTrigSrc(int trig_line, int clear_line, int event_num)
{
  if(trig_line < 0)
  {
    return 1;
  }

  if(clear_line < 0)
  {
    return 1;
  }

  if(event_num < 1 || event_num > 15)
  {
    return 1;
  }

  int res;
  enum PcdTriggerLine trig_line_enum, clear_line_enum;
  trig_line_enum = intToLine(trig_line);
  clear_line_enum = intToLine(clear_line);
  res = pcd_sen_set_trigger_source(device, trig_line_enum, clear_line_enum, event_num);

  return res;
}

// prepare dma transfer and trigger state to acquire data from sensor
int piezoSensor::pcd_sen_read_data_with_trigger(PcdSensorSample *storage, uint32_t samples, unsigned int timeout, int trig_type, int rearm)
{
  bool auto_rearm;
  PcdTriggerType trig_type_sens;
  int res;

  if (!device || !storage)
  {
    return PCD_ERROR_NULL_POINTER;
  }
  if (samples == 0)
  {
    return PCD_ERROR_OUT_OF_RANGE;
  }

  // Prepare DMA
  res = pcd_sen_read_prepare(device, samples);
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  // Set trigger
  trig_type_sens = valueToEnum(trig_type);
  auto_rearm = rearm?true:false;
  res = pcd_sen_trigger_arm(device, trig_type_sens, auto_rearm);
  // verify if trigger was set
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  //Check if transfer has finished
  for (unsigned int i = 0; i <= timeout; i++)
  {
    bool running;
    res = pcd_sen_read_check(device, &running);
    if (res != PCD_ERROR_NONE)
    {
      return res;
    }
    if (!running)
    {
      break;
    }
    usleep(1000);
  }

  // Cleaning
  res = pcd_sen_read_finalize(device);
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  // Read from memory
  res = pcd_sen_read_transfer(device, storage, samples);
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  return PCD_ERROR_NONE;
}

// read data from sensor
int piezoSensor::getSensData(int samples, int timeout, int chA, int chB, epicsFloat32** pvData, int trig_type, int rearm)
{
  int res;
  size_t req_size = samples * sizeof(PcdSensorSample);
  size_t alloc_size = MAX_ARRAY_LENGTH * ((req_size + MAX_ARRAY_LENGTH - 1) / MAX_ARRAY_LENGTH);
  PcdSensorSample *ss = (PcdSensorSample*)aligned_alloc(MAX_ARRAY_LENGTH, alloc_size);
  // download data from piezo driver
  res = pcd_sen_read_data_with_trigger(ss, samples, timeout, trig_type, rearm);
  if (res == PCD_ERROR_NONE)
  {
    for (int s_no = 0; s_no<samples; s_no++)
    {
      PcdSensorSample *s = &ss[s_no];
      // actuator
      if (chA)
      {
        pvData[0][s_no] = ((s->ch_a * ADC_REF_VOLT / (1 << ADC_BITS) - CURR_OFFSET) / CURR_SENS); // chan A current
        pvData[1][s_no] = s->ch_b * ADC_REF_VOLT / (1 << ADC_BITS) * VOLT_DIV; // chan A voltage
      }
      //sensor
      else
      {
        pvData[0][s_no] = (s->ch_a * ADC_REF_VOLT / (1 << ADC_BITS) - CURR_OFFSET) / CURR_SENS; // chan A current
        pvData[1][s_no] = s->ch_b * ADC_REF_VOLT / (1 << ADC_BITS); // chan A voltage
      }

      // actuator
      if (chB)
      {
        pvData[3][s_no] = ((s->ch_d * ADC_REF_VOLT / (1 << ADC_BITS) - CURR_OFFSET) / CURR_SENS); // chan B current
        pvData[2][s_no] = s->ch_c * ADC_REF_VOLT / (1 << ADC_BITS) * VOLT_DIV; // chan B voltage
      }
      // sensor
      else
      {
        pvData[3][s_no] = (s->ch_d * ADC_REF_VOLT / (1 << ADC_BITS) - CURR_OFFSET) / CURR_SENS; // chan B current
        pvData[2][s_no] = s->ch_c * ADC_REF_VOLT / (1 << ADC_BITS); // chan B voltage
      }
    }
  }
  free(ss);
  return res;
}

// convert in valye to trigger enum
PcdTriggerType piezoSensor::valueToEnum(int enum_int)
{
  switch (enum_int)
  {
  case 0:
    return PCD_TR_NONE;
  case 1:
    return PCD_TR_FREE;
  case 2:
    return PCD_TR_RISING_EDGE;
  case 3:
    return PCD_TR_FALLING_EDGE;
  case 4:
    return PCD_TR_ANY_EDGE;
  }
  return PCD_TR_NONE;
}

// convert int to line enum
PcdTriggerLine piezoSensor::intToLine(int line)
{
  switch (line)
  {
  case 0:
    return PCD_TL_RX17;
  case 1:
    return PCD_TL_TX17;
  case 2:
    return PCD_TL_RX18;
  case 3:
    return PCD_TL_TX18;
  case 4:
    return PCD_TL_RX19;
  case 5:
    return PCD_TL_TX19;
  case 6:
    return PCD_TL_RX20;
  case 7:
    return PCD_TL_TX20;
  }
  return PCD_TL_RX17;
}
