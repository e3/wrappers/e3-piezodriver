#ifndef _DIAG_PROC_H_
#define _DIAG_PROC_H_

#include "libpcd.h"
#include <string>


enum Statuses {
  StatusOK,
  StatusOverVoltage,
  StatusOverCurrent,
  StatusOverTemp,
  StatusHardFault,
  StatusNoPower,
  StatusNoCable,
  StatusNotKnown,
};

enum PAStatuses {
  PAStatusOK,
  PAStatusOTW,
  PAStatusC1,
  PAStatusC2,
  PAStatusFLT,
  PAStatusNotKnown,
};

class DiagChannel
{
public:
  std::string mode;
  double temperature;
  double voltageAC;
  double voltageDC;
  double voltageRMS;
  double currentAC;
  double currentDC;
  double currentRMS;
  std::string status;
  std::string paFlags;
  double idResistor;
  std::string getDetailedStatus();
  std::string getDetailedPAStatus();
protected:

private:
  Statuses resolveStatus();
  PAStatuses resolvePAStatus();
};


class DiagCpu
{
public:
  void config(xildev *dev);
  int readData();
  DiagChannel chanA;
  DiagChannel chanB;
protected:

private:
  xildev *device;

  void parse();
  int diag_talk(char *output);
  void msleep(unsigned int milliseconds);
  DiagChannel parseChannelParams(std::string data);
};


#endif
