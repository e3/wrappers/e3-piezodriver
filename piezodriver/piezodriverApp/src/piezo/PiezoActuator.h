#ifndef _PIEZO_ACT_H_
#define _PIEZO_ACT_H_

#include "libpcd.h"

#include <epicsTypes.h>


class piezoActuator
{
public:
  piezoActuator();
  ~piezoActuator();
  void configChan(int channel, xildev *dev);
  int disableTrig(int *arm_state, int *trig_state);
  int enableTrig(int trig_state, int arm_state);

  int getProtection(int *if_tripped);
  int getDesiredChanDir(int *dir);
  int getActualChanDir(int *dir);
  int getTrigDel(double *delay);
  int getTrigOccur(int *occurred);
  int getTrigType(int *trig_type, int *rearm);
  int getTrigSrc(int *trig_line, int *stop_line, int *evt_no);
  int getTrigPerLine(int *trig_count);

  int setTrigDel(double delay);
  int setTrigArm(int type, int arm);
  int setWave(double freq, double amp, int type, double offset, int tr, int tf);
  int setWaveFromFile(int loaded_samples, float *data);
  int resetProtection();
  int setDir(int state);
  int setTrigSrc(int trig_line, int clear_line, int event_num);

  void preparePreview(double freq, double amp, int type, double offset, int num_samples, int tr, int tf);
  void preparePreview(epicsFloat32 *samples, double freq, double amp, int type, double offset, int num_samples, int tr, int tf);

protected:

private:
  int channel;
  xildev *device;

  epicsInt16 *samplesBuf;

  PcdTriggerType valueToEnum(int enum_int);
  PcdTriggerLine intToLine(int line);
  void initPreview(double *samples, double freq, double amp, int type, double offset, int samples_num, int tr, int tf);
};
#endif
