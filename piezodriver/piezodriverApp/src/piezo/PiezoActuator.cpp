#include <stdio.h>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include <string.h>

#include "PiezoActuator.h"
#include "libpcd.h"

#define MAX_ACT_INPUT_SIZE      16384 // in bytes
#define MAX_ACT_INPUT_SAMPLES   MAX_ACT_INPUT_SIZE / sizeof(epicsInt16)
#define PAGE_SIZE               4096

const double ACTUATOR_GAIN = (0x7FFF / 187.4);

piezoActuator::piezoActuator()
{
  posix_memalign((void **)&samplesBuf, PAGE_SIZE /*alignment */ , MAX_ACT_INPUT_SIZE);
}

piezoActuator::~piezoActuator()
{
  free(samplesBuf);
}

void piezoActuator::configChan(int chan, xildev *dev)
{
  channel = chan;
  device = dev;
}

// read trigger state, return it by reference and disable trigger
int piezoActuator::disableTrig(int *arm_state, int *trig_state)
{
  int res = PCD_ERROR_NONE, res1 = PCD_ERROR_NONE, res2 = PCD_ERROR_NONE, res3 = PCD_ERROR_NONE;
  enum PcdTriggerType trig;
  bool auto_rearm;

  // get trigger state
  res1 = pcd_act_get_trigger_type(device, channel, &trig, &auto_rearm);
  if (res1 == PCD_ERROR_NONE)
  {
    *arm_state = auto_rearm?1:0;
    *trig_state = (int)trig;
    if (trig != PCD_TR_NONE)
    {
      // disable trigger
      res2 = pcd_act_trigger_arm(device, channel, PCD_TR_NONE, false);
      res3 = pcd_act_dma_wait_idle(device, channel);
    }
  }
  if (res1 != PCD_ERROR_NONE)
  {
    res = res1;
  }
  else if (res2 != PCD_ERROR_NONE)
  {
    res = res2;
  }
  else if (res3 != PCD_ERROR_NONE)
  {
    res = res3;
  }
  return res;
}

// enable trigger and set it to previous state
int piezoActuator::enableTrig(int trig_state, int arm_state)
{
  int res = 0;
  bool arm;
  enum PcdTriggerType trig_state_enum;

  arm = arm_state?true:false;
  trig_state_enum = valueToEnum(trig_state);
  res = pcd_act_trigger_arm(device, channel, trig_state_enum, arm);

  return res;
}

// read protection circuit status
int piezoActuator::getProtection(int *if_tripped)
{
  int res;
  bool state;
  res = pcd_mcu_get_prot(device, channel, &state);
  *if_tripped = state?1:0;
  return res;
}

// read desired channel direction
int piezoActuator::getDesiredChanDir(int *dir)
{
  int res;
  bool desired_dir;
  res = pcd_mcu_get_dir(device, channel, &desired_dir);
  *dir = desired_dir?1:0;
  return res;
}

// read actual channel direction
int piezoActuator::getActualChanDir(int *dir)
{
  int res;
  bool actual_dir;
  res = pcd_mcu_is_actuator(device, channel, &actual_dir);
  *dir = actual_dir?1:0;
  return res;
}

// read trigger delay
int piezoActuator::getTrigDel(double *delay)
{
  int res;
  res = pcd_act_get_trigger_delay(device, channel, delay);
  return res;
}

// check if trigger occurred
int piezoActuator::getTrigOccur(int *occurred)
{
  int res;
  bool triggered;
  res = pcd_act_was_triggered(device, channel, &triggered);
  *occurred = triggered?1:0;
  return res;
}

// read trigger type and auto-rearm status
int piezoActuator::getTrigType(int *trig_type, int *rearm)
{
  int res;
  bool auto_rearm_act;
  enum PcdTriggerType trig_enum_act;

  res = pcd_act_get_trigger_type(device, channel, &trig_enum_act, &auto_rearm_act);

  *trig_type = (int)trig_enum_act;
  *rearm = auto_rearm_act?1:0;

  return res;
}

// read MLVDS configuration
int piezoActuator::getTrigSrc(int *trig_line, int *stop_line, int *evt_no)
{
  int res;
  enum PcdTriggerLine trig_line_enum, stop_line_enum;
  uint8_t evt;

  //res  = pcd_act_get_trigger_source(device, &trig_line_enum, &stop_line_enum, evt_no);
  res  = pcd_act_get_trigger_source(device, &trig_line_enum, &stop_line_enum, &evt);
  *trig_line = (int)trig_line_enum;
  *stop_line = (int)stop_line_enum;
  *evt_no = int(evt);

  return res;
}

// read number of triggers per trigger line
int piezoActuator::getTrigPerLine(int *trig_count)
{
  int res;
  uint8_t trig;
  res = pcd_act_get_triggers_per_line(device, &trig);
  *trig_count = (int)trig;
  return res;
}

// set trigger delay
int piezoActuator::setTrigDel(double delay)
{
  int res;
  delay = delay / 1000;
  res = pcd_act_set_trigger_delay(device, channel, delay);
  return res;
}

// set trigger type and auto-rearm
int piezoActuator::setTrigArm(int type, int arm)
{
  int res;
  enum PcdTriggerType trig_state_enum_act = valueToEnum(type);
  bool arm_act = arm?true:false;
  res = pcd_act_trigger_arm(device, channel, trig_state_enum_act, arm_act);
  return res;
}

// load waveform from file (pheobus with script to read csv) to piezo driver
int piezoActuator::setWaveFromFile(int loaded_samples, float *data)
{
  int res = PCD_ERROR_NONE, res1 = PCD_ERROR_NONE, res2 = PCD_ERROR_NONE, res3 = PCD_ERROR_NONE, res4 = PCD_ERROR_NONE, res5 = PCD_ERROR_NONE;
  // read state of the trigger autorearm parameter and disable trigger
  int arm_state;
  int trig_state_act;
  res = disableTrig(&arm_state, &trig_state_act);
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  for (int i = 0; i < MAX_ACT_INPUT_SAMPLES; i++)
  {
    if (i < loaded_samples)
    {
      int v = 0;
      v = round(data[i] * ACTUATOR_GAIN);
      if (v > INT16_MAX)
        v = INT16_MAX;
      else if (v < INT16_MIN)
        v = INT16_MIN;
      samplesBuf[i] = v;
    }
    else
    {
      samplesBuf[i] = 0;
    }
  }

  // Load waveform to piezo
  res1 = pcd_act_dma_stop(device, channel);

  res2 = pcd_act_write_data(device, channel, samplesBuf, MAX_ACT_INPUT_SAMPLES);

  res3 = pcd_act_set_sample_count(device, channel, loaded_samples);

  res4 = pcd_act_dma_start(device, channel, loaded_samples);

  res5 = enableTrig(trig_state_act, arm_state);

  // return error that occurred first
  if (res1 != PCD_ERROR_NONE)
  {
    res = res1;
  }
  else if (res2 != PCD_ERROR_NONE)
  {
    res = res2;
  }
  else if (res3 != PCD_ERROR_NONE)
  {
    res = res3;
  }
  else if (res4 != PCD_ERROR_NONE)
  {
    res = res4;
  }
  else if (res5 != PCD_ERROR_NONE)
  {
    res = res5;
  }

  return res;
}

// load waveform generated from predefined shapes to piezo driver
int piezoActuator::setWave(double act_freq, double act_amp, int act_type, double offset, int tr, int tf)
{
  int res = PCD_ERROR_NONE, res1 = PCD_ERROR_NONE, res2 = PCD_ERROR_NONE, res3 = PCD_ERROR_NONE, res4 = PCD_ERROR_NONE, res5 = PCD_ERROR_NONE;
  // read state of the trigger autorearm parameter and disable trigger
  int arm_state;
  int trig_state_act;
  res = disableTrig(&arm_state, &trig_state_act);
  if (res != PCD_ERROR_NONE)
  {
    return res;
  }

  // Prepare waveform
  int num_samples = round(1e6 / act_freq);
  // make it even
  num_samples &= ~1;
  preparePreview(act_freq, act_amp, act_type, offset, num_samples, tr, tf);

  // Load waveform to piezo
  res1 = pcd_act_dma_stop(device, channel);

  res2 = pcd_act_write_data(device, channel, samplesBuf, MAX_ACT_INPUT_SAMPLES);

  res3 = pcd_act_set_sample_count(device, channel, num_samples);

  res4 = pcd_act_dma_start(device, channel, num_samples);

  // Bring back previous state of trigger
  res5 = enableTrig(trig_state_act, arm_state);

  // return error that occurred first
  if (res1 != PCD_ERROR_NONE)
  {
    res = res1;
  }
  else if (res2 != PCD_ERROR_NONE)
  {
    res = res2;
  }
  else if (res3 != PCD_ERROR_NONE)
  {
    res = res3;
  }
  else if (res4 != PCD_ERROR_NONE)
  {
    res = res4;
  }
  else if (res5 != PCD_ERROR_NONE)
  {
    res = res5;
  }
  return res;
}

// reset protection circuit
int piezoActuator::resetProtection()
{
  int res;
  res = pcd_mcu_reset_prot(device, channel);
  return res;
}

// set channel director (sensor/actuator)
int piezoActuator::setDir(int state)
{
  int res;
  bool direction = state?true:false;
  res = pcd_mcu_set_dir(device, channel, direction);
  return res;
}

// convert trigger enum to int value
PcdTriggerType piezoActuator::valueToEnum(int enum_int)
{
  switch (enum_int)
  {
  case 0:
    return PCD_TR_NONE;
  case 1:
    return PCD_TR_FREE;
  case 2:
    return PCD_TR_RISING_EDGE;
  case 3:
    return PCD_TR_FALLING_EDGE;
  case 4:
    return PCD_TR_ANY_EDGE;
  }
  return PCD_TR_NONE;
}

PcdTriggerLine piezoActuator::intToLine(int line)
{
  switch (line)
  {
  case 0:
    return PCD_TL_RX17;
  case 1:
    return PCD_TL_TX17;
  case 2:
    return PCD_TL_RX18;
  case 3:
    return PCD_TL_TX18;
  case 4:
    return PCD_TL_RX19;
  case 5:
    return PCD_TL_TX19;
  case 6:
    return PCD_TL_RX20;
  case 7:
    return PCD_TL_TX20;
  }
  return PCD_TL_RX17;
}

// generate waveform preview from predefined shapes and values (amplitude, frequency, offset) int values for piezo driver
void piezoActuator::preparePreview(double freq, double amp, int type, double offset, int num_samples, int tr, int tf)
{
  double *dblsamples = (double*)malloc(num_samples * sizeof(double));
  initPreview(dblsamples, freq, amp, type, offset, num_samples, tr, tf);

  for (int i = 0; i < MAX_ACT_INPUT_SAMPLES; i++)
  {
    if (i < num_samples)
    {
      int v = round((dblsamples[i] * amp + offset) * ACTUATOR_GAIN);
      if (v > INT16_MAX)
        v = INT16_MAX;
      else if (v < INT16_MIN)
        v = INT16_MIN;
      samplesBuf[i] = v;
    }
    else
    {
      int v = round(offset * ACTUATOR_GAIN);
      if (v > INT16_MAX)
        v = INT16_MAX;
      else if (v < INT16_MIN)
        v = INT16_MIN;
      samplesBuf[i] = v;
    }
  }
  free(dblsamples);
}

// generate waveform preview from predefined shapes and values (amplitude, frequency, offset) float values for phoebus
void piezoActuator::preparePreview(epicsFloat32 *samples, double freq, double amp, int type, double offset, int num_samples, int tr, int tf)
{
  double *dblsamples = (double*)malloc(num_samples * sizeof(double));
  initPreview(dblsamples, freq, amp, type, offset, num_samples, tr, tf);
  for (int i = 0; i < num_samples; i++)
  {
    samples[i] = (dblsamples[i] * amp + offset) * ACTUATOR_GAIN;
  }
  free(dblsamples);
}

// generate waveform from predefined shapes
void piezoActuator::initPreview(double* dblsamples, double freq, double amp, int type, double offset, int num_samples, int tr, int tf)
{
  enum
  {
    W_SIN,
    W_TRI,
    W_SQR,
    W_TRP
  } wave;

  if (type == 1)
  {
    wave = W_SIN;
  }
  else if (type == 2)
  {
    wave = W_TRI;
  }
  else if (type == 3)
  {
    wave = W_SQR;
  }
  else if (type == 4)
  {
    wave = W_TRP;
  }
  else
  {
    return;
  }

  int num_samples2 = num_samples / 2;

  switch (wave)
  {
  case W_SIN:
  {
    double w = 2.0 * M_PI / (double)num_samples;
    for (int i = 0; i < num_samples; i++)
    {
      dblsamples[i] = sinf(w*i);
    }
  }
  break;
  case W_TRI:
  {
    double m = 2.0 / (double)(num_samples2 - 1);
    for (int i = 0; i < num_samples2; i++)
    {
      dblsamples[i] = dblsamples[num_samples - 1 - i] = 1.0 - (double)i * m;
    }
  }
  break;
  case W_SQR:
  {
    for (int i = 0; i < num_samples2; i++)
    {
      dblsamples[i] =- 1.0;
      dblsamples[i + num_samples2] = 1.0;
    }
  }
  break;
  case W_TRP:
  {
    double delta_amp_tr = 1.0 / double(tr);
    int td = num_samples - tr - tf;
    if (td < 1)
    {
      for (int i = 0; i < num_samples + 1; i++)
      {
        dblsamples[i] = 0.0;
      }
      break;
    }
    double delta_amp_td = 1.0 / double(td);
    for (int i = 0; i < num_samples; i++)
    {
      if (i < tr)
      {
        dblsamples[i] = double(i) * delta_amp_tr;
      }
      else if (i >= tr && i < (tf + tr))
      {
        dblsamples[i] = 1;
      }
      else
      {
        dblsamples[i] = 1 - (i - tf - tr) * delta_amp_td;
      }
    }
  }
  break;
  default:
    break;
  }
}

int piezoActuator::setTrigSrc(int trig_line, int clear_line, int event_num)
{
  if(trig_line < 0)
  {
    return 1;
  }

  if(clear_line < 0)
  {
    return 1;
  }

  if(event_num < 1 || event_num > 15)
  {
    return 1;
  }

  int res;
  enum PcdTriggerLine trig_line_enum, clear_line_enum;
  trig_line_enum = intToLine(trig_line);
  clear_line_enum = intToLine(clear_line);
  res = pcd_act_set_trigger_source(device, trig_line_enum, clear_line_enum, event_num);

  return res;
}
