#include <stdio.h>
#include <iostream>
#include <cstring>
#include <sstream>
#include <regex>

#include "libpcd.h"
#include "DiagnosticProcessor.h"


const char *CMD = "s";

Statuses DiagChannel::resolveStatus()
{
  if(status.compare("OK") == 0)
  {
    return StatusOK;
  }
  else if (status.compare("overvoltage") == 0)
  {
    return StatusOverVoltage;
  }
  else if (status.compare("overcurrent") == 0)
  {
    return StatusOverCurrent;
  }
  else if (status.compare("overtemperature") == 0)
  {
    return StatusOverTemp;
  }
  else if (status.compare("hardware fault") == 0)
  {
    return StatusHardFault;
  }
  else if (status.compare("no power") == 0)
  {
    return StatusNoPower;
  }
  else if (status.compare("cable not present") == 0)
  {
    return StatusNoCable;
  }
  return StatusNotKnown;
}

PAStatuses DiagChannel::resolvePAStatus()
{
  if(paFlags.compare("<NONE>") == 0)
  {
    return PAStatusOK;
  }
  else if (paFlags.compare("OTW") == 0)
  {
    return PAStatusOTW;
  }
  else if (paFlags.compare("C1") == 0)
  {
    return PAStatusC1;
  }
  else if (paFlags.compare("C2") == 0)
  {
    return PAStatusC2;
  }
  else if (paFlags.compare("FLT") == 0)
  {
    return PAStatusFLT;
  }
  return PAStatusNotKnown;
}


void DiagCpu::config(xildev *dev)
{
  device = dev;
}

void DiagCpu::msleep(unsigned int milliseconds)
{
  struct timespec st;
  st.tv_sec = milliseconds / 1000;
  st.tv_nsec = (milliseconds % 1000) * 1000000L;
  nanosleep(&st, NULL);
}

int DiagCpu::diag_talk(char *output)
{
  uint8_t buf[1024];
  int tx_r, rx_r, status = 0;
  snprintf((char*)buf, sizeof(buf), "%s\r\n", CMD);
  tx_r = pcd_mcu_tx(device, (uint8_t*)buf, strlen((char*)buf));
  if(tx_r != 0)
  {
    status = tx_r;
  }

  uint16_t charCnt = 0;
  msleep(50);
  do
  {
    msleep(40);
    rx_r = pcd_mcu_rx(device, buf, sizeof(buf) - 1);
    if(rx_r < 0)
    {
      status = rx_r;
    }
    if(rx_r > 0)
    {
      buf[rx_r] = '\0';
      memcpy(output + charCnt, (char*)buf, sizeof(char)*rx_r);
      charCnt += rx_r;
    }
  }
  while(rx_r > 0);
  return status;
}

std::string DiagChannel::getDetailedStatus()
{
  switch(resolveStatus())
  {
    case StatusOK: {
      return "Channel active (protection not tripped)";
    }
    case StatusOverVoltage: {
      return "Channel voltage exceeded max of 190V";
    }
    case StatusOverCurrent: {
      return "RMS current exceeded limit of 3A";
    }
    case StatusOverTemp: {
      return "Power amplifier over-temperature";
    }
    case StatusHardFault: {
      return "Power amplifier general fault";
    }
    case StatusNoPower: {
      return "High voltage power supply missing";
    }
    case StatusNoCable: {
      return "Piezo-element cable not detected";
    }
    case StatusNotKnown: {
      return "Not known error";
    }
  }
  return "";
}

std::string DiagChannel::getDetailedPAStatus()
{
  switch(resolvePAStatus())
  {
    case PAStatusOK: {
      return "No warnings";
    }
    case PAStatusOTW: {
      return "over-temperature warning";
    }
    case PAStatusC1: {
      return "signal clipping in positive out driver";
    }
    case PAStatusC2: {
      return "signal clipping in negative out driver";
    }
    case PAStatusFLT: {
      return "general fault (or actuator is disabled)";
    }
    case PAStatusNotKnown: {
      return "Not known error";
    }
  }
  return "";
}

int DiagCpu::readData()
{
  char buf[2048];
  int status = diag_talk(buf);
  if (status != 0)
  {
    return status;
  }

  std::string data = buf;
  size_t chB_start = data.find("Channel B");
  if (chB_start == std::string::npos) {
    return 1;
  }

  std::string chA, chB;
  chA = data.substr(0, chB_start);
  chB = data.substr(chB_start);
  chanA = parseChannelParams(chA);
  chanB = parseChannelParams(chB);
  return 0;
}

DiagChannel DiagCpu::parseChannelParams(std::string data)
{
  DiagChannel channel;
  std::istringstream iss(data);
  std::string line;

  while (std::getline(iss, line))
  {
    if (line.empty()) continue;

    std::istringstream lineStream(line);
    std::string label;
    lineStream >> label;

    if (label == "Mode:")
    {
      lineStream >> channel.mode;
    }
    else if (label == "Temp:")
    {
      lineStream >> channel.temperature;
    }
    else if (label == "Voltage:")
    {
      lineStream >> label; // Consume the colon

      std::string ac, dc, rms;
      std::getline(iss, ac);
      std::istringstream acStream(ac);
      acStream >> channel.voltageAC;

      std::getline(iss, dc);
      std::istringstream dcStream(dc);
      dcStream >> channel.voltageDC;

      std::getline(iss, rms);
      std::istringstream rmsStream(rms);
      rmsStream >> channel.voltageRMS;
    }
    else if (label == "Current:")
    {
      lineStream >> label; // Consume the colon

      std::string ac, dc, rms;
      std::getline(iss, ac);
      std::istringstream acStream(ac);
      acStream >> channel.currentAC;

      std::getline(iss, dc);
      std::istringstream dcStream(dc);
      dcStream >> channel.currentDC;

      std::getline(iss, rms);
      std::istringstream rmsStream(rms);
      rmsStream >> channel.currentRMS;
    }
    else if (label == "Status:")
    {
      lineStream >> channel.status;
    }
    else if (label == "PA" && line.find("flags:") != std::string::npos)
    {
      lineStream >> label;
      lineStream >> channel.paFlags;
    }
    else if (label == "ID" && line.find("Resistor:") != std::string::npos)
    {
      lineStream >> label;
      lineStream >> channel.idResistor;
    }
  }
  return channel;
}
