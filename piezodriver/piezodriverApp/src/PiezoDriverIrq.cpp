#include <math.h>
#include <unistd.h>
#include <ratio>
#include <ctime>
#include <chrono>
#include <utility>

#include "PiezoDriverAsynPortDriver.h"

const int VOLT_DIV = 61;
const double CURR_SENS = 0.066;
const double CURR_OFFSET = 1.67;
const double ADC_REF_VOLT = 4.096 * 2;
const int ADC_BITS = 16;
const int MIN_DROP_SAMP = 0;
const int MAX_DROP_SAMP = 255;
const int MIN_AMP = 0;
const int MAX_AMP = 10000;
const int MIN_FREQ = 10;
const int MAX_FREQ = 100000;
const int MIN_OFFSET = -200;
const int MAX_OFFSET = 200;
const int BUFFOR_SIZE = 1024;
const int MIN_ARRAY_LENGTH = 128;
const int MAX_ARRAY_LENGTH = 8192;
const int MAX_STRING_LENGTH = 24;
const int ACQ_CHANNELS = 4;
const int MON_SLEEP_SEC = 10; //in seconds

using namespace std;

void PiezoDriverIrq(void *drvPvt)
{
  PiezoDriverAsynPortDriver *pPvt = (PiezoDriverAsynPortDriver *)drvPvt;
  pPvt->PiezoDriverIrq();
}

void PiezoDriverMonitor(void *drvPvt)
{
  PiezoDriverAsynPortDriver *pPvt = (PiezoDriverAsynPortDriver *)drvPvt;
  pPvt->PiezoDriverMonitor();
}

void PiezoDriverAsynPortDriver::PiezoDriverMonitor(void)
{
  xildev *testDev = NULL;

  while (terminateWorkers == 0) {
    testDev = xil_open_device(deviceName);
    if (!testDev){
      asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "Device not connected, trying to reconnect in %d second...\n", MON_SLEEP_SEC);
      deviceConnected = false;
    }
    else if (!deviceConnected && testDev){
      deviceConnected = initializeDevice();
      if (deviceConnected)
      {
        configureActuators();
        configureSensor();
      }
    }
    xil_close_device(testDev);
    testDev = NULL;
    setIntegerParam(P_DEV_CON_RD, (int) deviceConnected);
    usleep(MON_SLEEP_SEC*1e6);
  }

  // termination handling
  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}

void PiezoDriverAsynPortDriver::PiezoDriverIrq(void)
{
  epicsMutexMustLock(terminateWorkersLock);
  asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "Starting IRQ thread\n");

  // variables for counting triggers
  std::chrono::steady_clock::time_point start;
  std::chrono::steady_clock::time_point end;

  int capturing_mode = 0;
  while (terminateWorkers==0)
  {
    epicsMutexUnlock(terminateWorkersLock);

    // Sleep and retry if device is not connected
    if (!deviceConnected){
      usleep(1e6);
      epicsMutexMustLock(terminateWorkersLock)
      continue;
    }

    // Actuators configuration
    // read callback
    // pcd tool -p
    int res;
    int tripA, tripB, actA, actB, desA, desB, typA, typB, armA, armB, occurA, occurB, trigSrc, clear, evt_no, trigPerLine;
    double delA, delB;

    res = chanA.getProtection(&tripA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_A.RST_PROT_RD, tripA);
      unlock();
    }
    res = chanB.getProtection(&tripB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_B.RST_PROT_RD, tripB);
      unlock();
    }

    res = chanA.getActualChanDir(&actA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_A_SWITCH.ACT_RD, actA);
      unlock();
    }

    res = chanB.getActualChanDir(&actB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_B_SWITCH.ACT_RD, actB);
      unlock();
    }

    res = chanA.getDesiredChanDir(&desA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_A_SWITCH.DES_RD, desA);
      unlock();
    }

    res = chanB.getDesiredChanDir(&desB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_B_SWITCH.DES_RD, desB);
      unlock();
    }

    res = chanA.getTrigDel(&delA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setDoubleParam(P_CHAN_A.TRIG_DELAY_RD, delA);
      unlock();
    }

    res = chanB.getTrigDel(&delB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setDoubleParam(P_CHAN_B.TRIG_DELAY_RD, delB);
      unlock();
    }

    res = chanA.getTrigOccur(&occurA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_A.TRIG_OCCUR_RD, occurA);
      unlock();
    }

    res = chanB.getTrigOccur(&occurB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_B.TRIG_OCCUR_RD, occurB);
      unlock();
    }

    res = chanA.getTrigType(&typA, &armA);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_A.TRIG_STATE_RD, typA);
      setIntegerParam(P_CHAN_A.TRIG_AUTO_REARM_RD, armA);
      unlock();
    }

    res = chanB.getTrigType(&typB, &armB);
    actAlarmProcedure(res, CHAN_B);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_CHAN_B.TRIG_STATE_RD, typB);
      setIntegerParam(P_CHAN_B.TRIG_AUTO_REARM_RD, armB);
      unlock();
    }

    // same parameters for both channels
    res = chanA.getTrigSrc(&trigSrc, &clear, &evt_no);
    actAlarmProcedure(res, CHAN_A);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_ACT_TRIGGER_LINE_RD, trigSrc);
      setIntegerParam(P_ACT_CLEAR_LINE_RD, clear);
      setIntegerParam(P_ACT_EVENT_NUM_RD, evt_no);
      unlock();
    }
    res = chanA.getTrigPerLine(&trigPerLine);
    if (res == PCD_ERROR_NONE)
    {
       lock();
       setIntegerParam(P_ACT_TRIGGER_PER_LINE_RD, trigPerLine);
       unlock();
    }

    // load actuator user parameters to piezo driver
    // pcd-tool -d
    if (P_ACTUATORS_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Updating actuators parameters\n");
      double delA_sp, delB_sp;
      int armA_sp, armB_sp, typA_sp, typB_sp, trigSrc, clear, evt_no;

      lock();
      getDoubleParam(P_CHAN_A.TRIG_DELAY_SP, &delA_sp);
      getIntegerParam(P_CHAN_A.TRIG_AUTO_REARM_SP, &armA_sp);
      getIntegerParam(P_CHAN_A.TRIG_STATE_SP, &typA_sp);
      getDoubleParam(P_CHAN_B.TRIG_DELAY_SP, &delB_sp);
      getIntegerParam(P_CHAN_B.TRIG_AUTO_REARM_SP, &armB_sp);
      getIntegerParam(P_CHAN_B.TRIG_STATE_SP, &typB_sp);
      getIntegerParam(P_ACT_TRIGGER_LINE_SP, &trigSrc);
      getIntegerParam(P_ACT_CLEAR_LINE_SP, &clear);
      getIntegerParam(P_ACT_EVENT_NUM_SP, &evt_no);
      unlock();

      res = chanA.setTrigDel(delA_sp);
      actAlarmProcedure(res, CHAN_A);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setDoubleParam(P_CHAN_A.TRIG_DELAY_RD, delA_sp);
        unlock();
      }
      res = chanA.setTrigArm(typA_sp, armA_sp);
      actAlarmProcedure(res, CHAN_A);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setIntegerParam(P_CHAN_A.TRIG_AUTO_REARM_RD, armA_sp);
        setIntegerParam(P_CHAN_A.TRIG_STATE_RD, typA_sp);
        unlock();
      }
      res = chanA.setTrigDel(delA_sp);
      actAlarmProcedure(res, CHAN_A);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setDoubleParam(P_CHAN_A.TRIG_DELAY_RD, delA_sp);
        unlock();
      }
      res = chanB.setTrigArm(typB_sp, armB_sp);
      actAlarmProcedure(res, CHAN_B);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setIntegerParam(P_CHAN_B.TRIG_AUTO_REARM_RD, armB_sp);
        setIntegerParam(P_CHAN_B.TRIG_STATE_RD, typB_sp);
        unlock();
      }
      res = chanB.setTrigDel(delB_sp);
      actAlarmProcedure(res, CHAN_B);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setDoubleParam(P_CHAN_B.TRIG_DELAY_RD, delB_sp);
        unlock();
      }

      res = chanA.setTrigSrc(trigSrc, clear, evt_no);
      actAlarmProcedure(res, CHAN_A);
      if (res == PCD_ERROR_NONE)
      {
        lock();
	setIntegerParam(P_ACT_TRIGGER_LINE_RD, trigSrc);
	setIntegerParam(P_ACT_CLEAR_LINE_RD, clear);
	setIntegerParam(P_ACT_EVENT_NUM_RD, evt_no);
	unlock();
      }
      callParamCallbacks();
      P_ACTUATORS_needs_update = false;
    }

    // Read sensor configuration
    // pcd tool -p
    int enable, samples, drops, sen_type, sen_arm, length, cnt, sen_occur, trigSrc_sen, clear_sen, evt_no_sen, trigPerLine_sen;
    double sen_del;
    res = sensor.getPreTrig(&enable);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_PRE_TRIG_RD, enable);
      unlock();
    }

    res = sensor.getSampleCnt(&samples);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_SAMPLE_COUNT_RD, samples);
      unlock();
    }

    res = sensor.getTrigDel(&sen_del);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setDoubleParam(P_SENS_DELAY_RD, sen_del * 1000);
      unlock();
    }

    res = sensor.getSampleDrop(&drops);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_SAMPLES_TO_DROP_RD, drops);
      unlock();
    }

    res = sensor.getTrigOccur(&sen_occur);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_TRIG_OCCUR_RD, sen_occur);
      unlock();
    }

    res = sensor.getTrigType(&sen_type, &sen_arm);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_TRIG_AUTO_REARM_RD, sen_arm);
      setIntegerParam(P_SENS_TRIG_STATE_RD, sen_type);
      unlock();
    }

    res = sensor.getTrigInfo(&length, &cnt);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setDoubleParam(P_TRIG_LEN_RD, length / 100.0f);
      setIntegerParam(P_TRIG_CNT_RD, cnt);
      unlock();
    }

    res = sensor.getTrigSrc(&trigSrc_sen, &clear_sen, &evt_no_sen);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_TRIGGER_LINE_RD, trigSrc_sen);
      setIntegerParam(P_SENS_CLEAR_LINE_RD, clear_sen);
      setIntegerParam(P_SENS_EVENT_NUM_RD, evt_no_sen);
      unlock();
    }

    res = sensor.getTrigPerLine(&trigPerLine_sen);
    sensAlarmProcedure(res);
    if (res == PCD_ERROR_NONE)
    {
      lock();
      setIntegerParam(P_SENS_TRIGGER_PER_LINE_RD, trigPerLine_sen);
      unlock();
    }

    // load sensor user parameters to piezo driver
    if (P_SENS_CONFIG_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Updating sensor parameters\n");
      double delay_sens;
      int drop_sp, samps_cnt, wave_buff, sens_pre, sen_typ, sen_arm, timeout, sen_trigSrc, sen_clear, sen_evt_no;

      lock();
      getDoubleParam(P_SENS_DELAY_SP, &delay_sens);
      unlock();
      // pcd tool -D
      res = sensor.setTrigDel(delay_sens / 1000);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setDoubleParam(P_SENS_DELAY_RD, delay_sens);
        unlock();
      }

      lock();
      getIntegerParam(P_SENS_SAMPLES_TO_DROP_SP, &drop_sp);
      unlock();

      //pcd tool -R
      res = sensor.setSampleDrop(drop_sp);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        setLimits(MIN_DROP_SAMP, MAX_DROP_SAMP, &drop_sp);
        lock();
        setIntegerParam(P_SENS_SAMPLES_TO_DROP_RD, drop_sp);
        unlock();
      }

      lock();
      getIntegerParam(P_SENS_SAMPLE_COUNT_SP, &samps_cnt);
      getIntegerParam(P_WAVE_BUFF_SP, &wave_buff);
      unlock();

      // pcd tool -S
      res = sensor.setSampleCnt(samps_cnt);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        setLimits(MIN_ARRAY_LENGTH, MAX_ARRAY_LENGTH, &samps_cnt);
        if (samps_cnt > wave_buff)
        {
          samps_cnt = wave_buff;
        }
        lock();
        setIntegerParam(P_SENS_SAMPLE_COUNT_RD, samps_cnt);
        unlock();
      }

      lock();
      getIntegerParam(P_SENS_PRE_TRIG_SP, &sens_pre);
      unlock();

      // pcd tool -P
      res = sensor.setPreTrig(sens_pre);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setIntegerParam(P_SENS_PRE_TRIG_RD, sens_pre);
        unlock();
      }

      lock();
      getIntegerParam(P_SENS_TRIG_STATE_SP, &sen_typ);
      getIntegerParam(P_SENS_TRIG_AUTO_REARM_SP, &sen_arm);
      unlock();

      // pcd tool -A -T
      res = sensor.setTrigArm(sen_typ, sen_arm);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setIntegerParam(P_SENS_TRIG_STATE_RD, sen_typ);
        setIntegerParam(P_SENS_TRIG_AUTO_REARM_RD, sen_arm);
        unlock();
      }

      lock();
      getIntegerParam(P_SENS_TIMEOUT_SP, &timeout);
      unlock();
      if (timeout > 0)
      {
        lock();
        setIntegerParam(P_SENS_TIMEOUT_RD, timeout);
        unlock();
      }

      // pcd-tool -L
      lock();
      getIntegerParam(P_SENS_TRIGGER_LINE_SP, &sen_trigSrc);
      getIntegerParam(P_SENS_CLEAR_LINE_SP, &sen_clear);
      getIntegerParam(P_SENS_EVENT_NUM_SP, &sen_evt_no);
      unlock();
      res = sensor.setTrigSrc(sen_trigSrc, sen_clear, sen_evt_no);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        lock();
        setIntegerParam(P_SENS_TRIGGER_LINE_RD, sen_trigSrc);
        setIntegerParam(P_SENS_CLEAR_LINE_RD, sen_clear);
        setIntegerParam(P_SENS_EVENT_NUM_RD, sen_evt_no);
	unlock();
      }

      P_SENS_CONFIG_needs_update = false;
      callParamCallbacks();
    }
    // Counting triggers in 5 second (to avoid doing it in second thread)
    // pcd tool -2
    int trig_len;
    if (time_start == true)
    {
      start = std::chrono::steady_clock::now();
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Performing 5s delay\n");
      res = sensor.getTrigInfo(&trig_len, &trig_cnt1);
      sensAlarmProcedure(res);
      if (res == PCD_ERROR_NONE)
      {
        time_start = false;
        time_pass = true;
      }
    }

    if (time_pass == true)
    {
      end = std::chrono::steady_clock::now();
      if (std::chrono::duration_cast < std::chrono::microseconds > (end - start).count() >= 5000000)
      {
        time_pass = false;
        res = sensor.getTrigInfo(&trig_len, &trig_cnt2);
        if (res == PCD_ERROR_NONE)
        {
          trig_cnt2 -= trig_cnt1;
          asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Triggers per second: %0.1f\n", trig_cnt2/5.0f);

          lock();
          setDoubleParam(P_TRIG_PER_SEC_RD, trig_cnt2/5.0f);
          unlock();
          trig_cnt1 = 0;
          trig_cnt2 = 0;
        }
        callParamCallbacks();
      }
    }
    //pcd-tool -w
    //Load waveform created in Phoebus to piezo driver
    if (P_CHAN_A_WAVE_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Loading predefined waveform to piezo driver - channel A\n");
      int load_type;

      lock();
      getIntegerParam(P_CHAN_A_WAVE.PREVIEW_TYPE_SP, &load_type);
      unlock();
      if (load_type == 0)
      {
        double act_freq, act_amp, act_offset;
        int act_type, trp_tr, trp_tf;

        lock();
        getDoubleParam(P_CHAN_A_WAVE.F_SP, &act_freq);
        getDoubleParam(P_CHAN_A_WAVE.A_SP, &act_amp);
        getIntegerParam(P_CHAN_A_WAVE.TYPE_SP, &act_type);
        getDoubleParam(P_CHAN_A_WAVE.OFFSET_SP, &act_offset);
        getIntegerParam(P_CHAN_A_WAVE.TRP_TR_SP, &trp_tr);
        getIntegerParam(P_CHAN_A_WAVE.TRP_TF_SP, &trp_tf);
        unlock();

        setLimits(MIN_FREQ, MAX_FREQ, &act_freq);
        setLimits(MIN_OFFSET, MAX_OFFSET, &act_offset);
        setLimits(MIN_AMP, MAX_AMP, &act_amp);

        // load samples to piezo driver
        res = chanA.setWave(act_freq, act_amp, act_type, act_offset, trp_tr, trp_tf);
        actAlarmProcedure(res, CHAN_A);

        lock();
        setDoubleParam(P_CHAN_A_WAVE.F_RD, act_freq);
        setDoubleParam(P_CHAN_A_WAVE.A_RD, act_amp);
        setIntegerParam(P_CHAN_A_WAVE.TYPE_RD, act_type);
        setDoubleParam(P_CHAN_A_WAVE.OFFSET_RD, act_offset);
        setIntegerParam(P_CHAN_A_WAVE.TRP_TR_RD, trp_tr);
        setIntegerParam(P_CHAN_A_WAVE.TRP_TF_RD, trp_tf);
        // Clear refresh flag
        setIntegerParam(P_CHAN_A.REFRESH_RD, 0);
        unlock();
      }

      P_CHAN_A_WAVE_needs_update = false;
      callParamCallbacks();
    }

    if (P_CHAN_B_WAVE_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Loading predefined waveform to piezo driver - channel B\n");
      int load_type;

      lock();
      getIntegerParam(P_CHAN_B_WAVE.PREVIEW_TYPE_SP, &load_type);
      unlock();

      if (load_type == 0)
      {
        double act_freq, act_amp, act_offset;
        int act_type, trp_tr, trp_tf;

        lock();
        getDoubleParam(P_CHAN_B_WAVE.F_SP, &act_freq);
        getDoubleParam(P_CHAN_B_WAVE.A_SP, &act_amp);
        getIntegerParam(P_CHAN_B_WAVE.TYPE_SP, &act_type);
        getDoubleParam(P_CHAN_B_WAVE.OFFSET_SP, &act_offset);
        getIntegerParam(P_CHAN_B_WAVE.TRP_TR_SP, &trp_tr);
        getIntegerParam(P_CHAN_B_WAVE.TRP_TF_SP, &trp_tf);
        unlock();

        setLimits(MIN_FREQ, MAX_FREQ, &act_freq);
        setLimits(MIN_OFFSET, MAX_OFFSET, &act_offset);
        setLimits(MIN_AMP, MAX_AMP, &act_amp);

        // load samples to piezo driver
        res = chanB.setWave(act_freq, act_amp, act_type, act_offset, trp_tr, trp_tf);
        actAlarmProcedure(res, CHAN_B);

        lock();
        setDoubleParam(P_CHAN_B_WAVE.F_RD, act_freq);
        setDoubleParam(P_CHAN_B_WAVE.A_RD, act_amp);
        setIntegerParam(P_CHAN_B_WAVE.TYPE_RD, act_type);
        setDoubleParam(P_CHAN_B_WAVE.OFFSET_RD, act_offset);
        setIntegerParam(P_CHAN_B_WAVE.TRP_TR_RD, trp_tr);
        setIntegerParam(P_CHAN_B_WAVE.TRP_TF_RD, trp_tf);
        // Clear refresh flag
        setIntegerParam(P_CHAN_B.REFRESH_RD, 0);
        unlock();
      }

      P_CHAN_B_WAVE_needs_update = false;
      callParamCallbacks();
    }

    // Load waveform from file to piezo driver
    if (P_CHAN_A_WAVE_LOAD_PD_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Loading waveform from file to piezo driver - channel A");
      int load_pd;

      lock();
      getIntegerParam(P_CHAN_A_WAVE.LOAD_PD_SP, &load_pd);
      unlock();

      if (load_pd == 1)
      {
        int load_type;
        lock();
        getIntegerParam(P_CHAN_A_WAVE.PREVIEW_TYPE_SP, &load_type);
        unlock();

        if (load_type == 1)
        {
          res = chanA.setWaveFromFile(number_of_samples_a, array_a);
          actAlarmProcedure(res, CHAN_A);
        }
      }

      P_CHAN_A_WAVE_LOAD_PD_needs_update = false;
    }

    if (P_CHAN_B_WAVE_LOAD_PD_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Loading waveform from file to piezo driver - channel B");
      int load_pd;

      lock();
      getIntegerParam(P_CHAN_B_WAVE.LOAD_PD_SP, &load_pd);
      unlock();

      if (load_pd == 1)
      {
        int load_type;
        lock();
        getIntegerParam(P_CHAN_B_WAVE.PREVIEW_TYPE_SP, &load_type);
        unlock();

        if (load_type == 1)
        {
          res = chanB.setWaveFromFile(number_of_samples_b, array_b);
          actAlarmProcedure(res, CHAN_B);
        }
      }

      P_CHAN_B_WAVE_LOAD_PD_needs_update = false;
    }

    //pcd-tool -w preview of waves created in css
    if (P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Creating preview array - channel A\n");
      double act_freq, act_amp, act_offset;
      int act_type, trp_tf, trp_tr;

      lock();
      getDoubleParam(P_CHAN_A_WAVE.F_SP, &act_freq);
      getDoubleParam(P_CHAN_A_WAVE.A_SP, &act_amp);
      getIntegerParam(P_CHAN_A_WAVE.TYPE_SP, &act_type);
      getDoubleParam(P_CHAN_A_WAVE.OFFSET_SP, &act_offset);
      getIntegerParam(P_CHAN_A_WAVE.TRP_TR_SP, &trp_tr);
      getIntegerParam(P_CHAN_A_WAVE.TRP_TF_SP, &trp_tf);
      unlock();

      setLimits(MIN_FREQ, MAX_FREQ, &act_freq);
      setLimits(MIN_OFFSET, MAX_OFFSET, &act_offset);
      setLimits(MIN_AMP, MAX_AMP, &act_amp);

      int num_samples = round(1e6 / act_freq);

      lock();
      setDoubleParam(P_CHAN_A_WAVE.F_RD, act_freq);
      setDoubleParam(P_CHAN_A_WAVE.A_RD, act_amp);
      setIntegerParam(P_CHAN_A_WAVE.TYPE_RD, act_type);
      setDoubleParam(P_CHAN_A_WAVE.OFFSET_RD, act_offset);
      setIntegerParam(P_CHAN_A_WAVE.TRP_TR_RD, trp_tr);
      setIntegerParam(P_CHAN_A_WAVE.TRP_TF_RD, trp_tf);
      unlock();

      lock();
      setIntegerParam(P_CHAN_A_WAVE.LOAD_SAMPLES_SUGG_RD, num_samples);
      setIntegerParam(P_CHAN_A.REFRESH_RD, 1);
      unlock();

      epicsFloat32 *samples = (epicsFloat32*)malloc(num_samples * sizeof(epicsFloat32));
      epicsInt32 *samples_x = (epicsInt32*)malloc(num_samples * sizeof(epicsInt32));

      //Generate preview array
      chanA.preparePreview(samples, act_freq, act_amp / ACTUATOR_GAIN, act_type, act_offset / ACTUATOR_GAIN, num_samples, trp_tr, trp_tf);

      for (int i = 0; i < num_samples; i++)
      {
        samples_x[i] = i + 1;
      }

      P_CHAN_A_WAVE_ARRAY_PREVIEW_needs_update = false;
      callParamCallbacks();
      doCallbacksFloat32Array(samples, num_samples, P_CHAN_A_WAVE.ARRAY_RD, 0);
      doCallbacksInt32Array(samples_x, num_samples, P_CHAN_A_WAVE.ARRAY_X_RD, 0);
      free(samples);
      free(samples_x);
    }

    if (P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Creating preview array - channel B\n");
      double act_freq, act_amp, act_offset;
      int act_type, trp_tr, trp_tf;

      lock();
      getDoubleParam(P_CHAN_B_WAVE.F_SP, &act_freq);
      getDoubleParam(P_CHAN_B_WAVE.A_SP, &act_amp);
      getIntegerParam(P_CHAN_B_WAVE.TYPE_SP, &act_type);
      getDoubleParam(P_CHAN_B_WAVE.OFFSET_SP, &act_offset);
      getIntegerParam(P_CHAN_B_WAVE.TRP_TR_SP, &trp_tr);
      getIntegerParam(P_CHAN_B_WAVE.TRP_TF_SP, &trp_tf);
      unlock();

      setLimits(MIN_FREQ, MAX_FREQ, &act_freq);
      setLimits(MIN_OFFSET, MAX_OFFSET, &act_offset);
      setLimits(MIN_AMP, MAX_AMP, &act_amp);

      int num_samples = round(1e6 / act_freq);

      lock();
      setDoubleParam(P_CHAN_B_WAVE.F_RD, act_freq);
      setDoubleParam(P_CHAN_B_WAVE.A_RD, act_amp);
      setIntegerParam(P_CHAN_B_WAVE.TYPE_RD, act_type);
      setDoubleParam(P_CHAN_B_WAVE.OFFSET_RD, act_offset);
      setIntegerParam(P_CHAN_B_WAVE.TRP_TR_RD, trp_tr);
      setIntegerParam(P_CHAN_B_WAVE.TRP_TF_RD, trp_tf);
      unlock();

      lock();
      setIntegerParam(P_CHAN_B_WAVE.LOAD_SAMPLES_SUGG_RD, num_samples);
      setIntegerParam(P_CHAN_B.REFRESH_RD, 1);
      unlock();

      epicsFloat32 *samples = (epicsFloat32*)malloc(num_samples * sizeof(epicsFloat32));
      epicsInt32 *samples_x = (epicsInt32*)malloc(num_samples * sizeof(epicsInt32));

      //Generate preview array
      chanB.preparePreview(samples, act_freq, act_amp/ACTUATOR_GAIN, act_type, act_offset/ACTUATOR_GAIN, num_samples, trp_tr, trp_tf);

      for (int i = 0; i < num_samples; i++)
      {
        samples_x[i] = i + 1;
      }

      P_CHAN_B_WAVE_ARRAY_PREVIEW_needs_update = false;
      callParamCallbacks();
      doCallbacksFloat32Array(samples, num_samples, P_CHAN_B_WAVE.ARRAY_RD, 0);
      doCallbacksInt32Array(samples_x, num_samples, P_CHAN_B_WAVE.ARRAY_X_RD, 0);
      free(samples);
      free(samples_x);
    }
    // pcd tool -m
    // switching mode of channel sensor/driver
    if (P_CHAN_A_SWITCH_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Switching channel A\n");
      int state;

      lock();
      getIntegerParam(P_CHAN_A_SWITCH.SP, &state);
      unlock();

      res = chanA.setDir(state);
      actAlarmProcedure(res, CHAN_A);

      P_CHAN_A_SWITCH_needs_update = false;
      callParamCallbacks();
    }

    if (P_CHAN_B_SWITCH_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Switching channel B\n");
      int state;

      lock();
      getIntegerParam(P_CHAN_B_SWITCH.SP, &state);
      unlock();

      res = chanB.setDir(state);
      actAlarmProcedure(res, CHAN_B);

      P_CHAN_B_SWITCH_needs_update = false;
      callParamCallbacks();
    }

    if (P_CHAN_A_RST_PROT_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Resetting protection circuit - channel A\n");

      res = chanA.resetProtection();
      actAlarmProcedure(res, CHAN_A);

      P_CHAN_A_RST_PROT_needs_update = false;
      callParamCallbacks();
    }

    if (P_CHAN_B_RST_PROT_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Resetting protection circuit - channel B\n");

      res = chanB.resetProtection();
      actAlarmProcedure(res, CHAN_B);

      P_CHAN_B_RST_PROT_needs_update = false;
      callParamCallbacks();
    }

    if (P_SENS_CAPTURING_MODE_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Changing capturing mode\n");
      int state;

      lock();
      getIntegerParam(P_SENS_CAPTURING_MODE_SP, &state);
      unlock();

      if (state == 1) //continuous mode
      {
        capturing_mode = 1;
      }
      else //single mode
      {
        capturing_mode = 0;
      }
      P_SENS_CAPTURING_MODE_needs_update = false;
    }

    if (capturing_mode == 1)
    {
      P_WAVEFORM_needs_update = true;
    }

    // read data from sensor
    // pcd-tool -N -G
    if (P_WAVEFORM_needs_update == true)
    {
      asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Reading data from sensor\n");
      int value, samples_to_drop, act_a, act_b, trig_type, rearm, timeout;
      lock();
      getIntegerParam(P_CHAN_A_SWITCH.ACT_RD, &act_a);
      getIntegerParam(P_CHAN_B_SWITCH.ACT_RD, &act_b);
      getIntegerParam(P_SENS_SWITCH_SP, &value);
      getIntegerParam(P_SENS_SAMPLES_TO_DROP_SP, &samples_to_drop);
      getIntegerParam(P_SENS_TIMEOUT_RD, &timeout);
      getIntegerParam(P_SENS_TRIG_AUTO_REARM_RD, &rearm);
      getIntegerParam(P_SENS_TRIG_STATE_RD, &trig_type);
      unlock();

      if (value != 0 || capturing_mode == 1)
      {
        int num_samples;

        lock();
        getIntegerParam(P_WAVE_BUFF_SP, &num_samples);
        unlock();

        // At the begining it needs time to load number of samples from parameter
        if (num_samples == 0)
        {
          num_samples = BUFFOR_SIZE;
        }

        pvData = new epicsFloat32 * [ACQ_CHANNELS];
        for (int i = 0; i < ACQ_CHANNELS; i++)
        {
          pvData[i] = new epicsFloat32[num_samples];
        }

        setLimits(MIN_ARRAY_LENGTH, MAX_ARRAY_LENGTH, &num_samples);
        res = sensor.getSensData(num_samples, timeout, act_a, act_b, pvData, trig_type, rearm);
        if (res == PCD_ERROR_NONE)
        {
          epicsFloat32* abChans = new epicsFloat32[2*num_samples];
          epicsFloat32* cdChans = new epicsFloat32[2*num_samples];
          // first current then voltage
          std::copy(pvData[0], pvData[0] + num_samples, abChans);
          std::copy(pvData[1], pvData[1] + num_samples, abChans+num_samples);
          std::copy(pvData[2], pvData[2] + num_samples, cdChans+num_samples);
          std::copy(pvData[3], pvData[3] + num_samples, cdChans);
          doCallbacksFloat32Array(pvData[0], num_samples, P_WAVE_CH_A_RD, 0);
          doCallbacksFloat32Array(pvData[1], num_samples, P_WAVE_CH_B_RD, 0);
          doCallbacksFloat32Array(abChans, 2*num_samples, P_WAVE_CH_AB_RD, 0);
          doCallbacksFloat32Array(pvData[2], num_samples, P_WAVE_CH_C_RD, 0);
          doCallbacksFloat32Array(pvData[3], num_samples, P_WAVE_CH_D_RD, 0);
          doCallbacksFloat32Array(cdChans, 2*num_samples, P_WAVE_CH_CD_RD, 0);
          callParamCallbacks();
          free(abChans);
          free(cdChans);
        }
        for (int i = 0; i < ACQ_CHANNELS; i++)
        {
          free(pvData[i]);
        }
        free(pvData);
      }
      P_WAVEFORM_needs_update = false;
    }

    callParamCallbacks();
    epicsMutexMustLock(terminateWorkersLock);
  }
  epicsMutexUnlock(terminateWorkersLock);

  // termination handling
  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}
