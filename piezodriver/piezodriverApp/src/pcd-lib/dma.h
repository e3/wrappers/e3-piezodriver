#ifndef DMA_H_
#define DMA_H_

#include "libxildrv.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

int dma_write_descriptor(xildev *dev, uint32_t bram_base_addr, uint8_t desc_no, uint8_t next_no, uint64_t addr, uint32_t length);

int dma_mm2s_reset(xildev *dev, uint32_t dma_s2mm_base_addr);
int dma_mm2s_start(xildev *dev, uint32_t dma_s2mm_base_addr, uint32_t bram_base_addr, uint8_t first_descriptor, uint8_t last_descriptor, bool cyclic);
int dma_mm2s_stop(xildev *dev, uint32_t dma_s2mm_base_addr);
int dma_mm2s_status(xildev *dev, uint32_t dma_base_addr, bool *running, bool *errors);

int dma_s2mm_reset(xildev *dev, uint32_t dma_s2mm_base_addr);
int dma_s2mm_start(xildev *dev, uint32_t dma_s2mm_base_addr, uint32_t bram_base_addr, uint8_t first_descriptor, uint8_t last_descriptor, bool cyclic);
int dma_s2mm_stop(xildev *dev, uint32_t dma_s2mm_base_addr);
int dma_s2mm_status(xildev *dev, uint32_t dma_base_addr, bool *running, bool *errors);

#ifdef __cplusplus
}
#endif

#endif /* DMA_H_ */
