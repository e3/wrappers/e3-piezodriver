#include "libpcd.h"
#include "uartlite.h"
#include "dma.h"
#include <unistd.h>
#include <math.h>

#define REGS_BASE_ADDRESS		0xC00000
#define UART_BASE_ADDRESS		0xC01000
#define SEN_DMA_BASE_ADDRESS	0xC02000
#define SEN_TRIG_BASE_ADDRESS	0xC03000
#define GEN0_DMA_BASE_ADDRESS	0xC04000
#define GEN0_REGS_BASE_ADDRESS	0xC05000
#define GEN1_DMA_BASE_ADDRESS	0xC06000
#define GEN1_REGS_BASE_ADDRESS	0xC07000
#define BRAM_BASE_ADDRESS		0xC08000

#define DEV_ADDR_SPACE_OFFSET	0x00000000


/* Sample memory:
128 KiB - total
- 64 KiB - sensor @ 0x000000
  - sample: 4 chanels x 16 bits = 8 B
  - 64 KiB / 8 B/s = 8192 samples
- 32 KiB - actuator A @ 0x010000
  - sample: 16 bits = 2 B
  - 32 KiB / 2 B/s = 16384 samples
- 32 KiB - actuator B @ 0x018000
  - sample: 16 bits = 2 B
  - 32 KiB / 2 B/s = 16384 samples
*/

#define SEN_BUF_ADDRESS			0x000000
#define SEN_MAX_SAMPLES			8192
#define ACT_BUF_ADDRESS			0x010000
#define ACT_BUF_STEP			0x008000
#define ACT_BUF_SAMPLES			16384


#define REG_PCD_CONTROL			(REGS_BASE_ADDRESS + 0x10)
#define REG_PCD_STATUS			(REGS_BASE_ADDRESS + 0x14)
#define REG_ACQ_TRIG			(REGS_BASE_ADDRESS + 0x18)
#define REG_GEN_TRIG			(REGS_BASE_ADDRESS + 0x1C)

#define SEN_REF_FREQ			100e6f

#define SEN_REG_CONFIG			0x00
#define SEN_REG_DELAY			0x04
#define SEN_REG_MISC			0x08
#define SEN_REG_STATUS			0x0C

#define SEN_BIT_PRE_TRIG		24
#define SEN_BIT_AUTO_REARM		23
#define SEN_BIT_FALLING_EDGE	22
#define SEN_BIT_RISING_EDGE		21
#define SEN_BIT_FREE_RUN		20
#define SEN_BIT_TRIG_DONE		0

#define ACT_REF_FREQ			80e6f	// MEM_ACLK

#define ACT_REG_CONFIG			0x00
#define ACT_REG_DELAY			0x04
#define ACT_REG_MISC			0x08
#define ACT_REG_STATUS			0x0C

#define ACT_BIT_FLUSH			24
#define ACT_BIT_AUTO_REARM		23
#define ACT_BIT_FALLING_EDGE	22
#define ACT_BIT_RISING_EDGE		21
#define ACT_BIT_FREE_RUN		20
#define ACT_BIT_TRIG_DONE		0
#define ACT_BIT_IDLE			1

#define PCD_RTM_FIRMWARE_ID		0x102

#define WRITE_REPS              64

static const uint32_t gen_regs_ba[ACT_CHANNELS] =
	{ GEN0_REGS_BASE_ADDRESS, GEN1_REGS_BASE_ADDRESS };
static const uint32_t gen_dma_ba[ACT_CHANNELS] =
	{ GEN0_DMA_BASE_ADDRESS, GEN1_DMA_BASE_ADDRESS };

//////////////////////////////////////////////////////////////////////////////
// Board initialization
//////////////////////////////////////////////////////////////////////////////

int pcd_init(xildev *dev)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val;
	int result = xil_read_reg(dev, 0x10, &reg_val);
	if(result != PCD_ERROR_NONE)
		return result;
	if(reg_val != PCD_RTM_FIRMWARE_ID)
		return PCD_ERROR_WRONG_FIRMWARE_ID;
	if(uart_lite_clear(dev, UART_BASE_ADDRESS) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Getting error messages
//////////////////////////////////////////////////////////////////////////////

const char * pcd_error_string(int error_code)
{
	switch(error_code)
	{
	case PCD_ERROR_NONE:				return "No error reported";
	case PCD_ERROR_NO_SUCH_CHANNEL:		return "Wrong channel number";
	case PCD_ERROR_REG_ACCESS:			return "Register access error";
	case PCD_ERROR_NULL_POINTER:		return "Null pointer passed to a function";
	case PCD_ERROR_OUT_OF_RANGE:		return "Parameter out of range";
	case PCD_ERROR_FILE_ACCESS:			return "Driver file access error";
	case PCD_ERROR_NO_RESPONSE:			return "No response from the device";
	case PCD_ERROR_DMA_TRANSFER_FAILED:	return "DMA transfer failed";
	case PCD_ERROR_WRONG_FIRMWARE_ID:	return "Unexpected value of firmware ID register";
	case PCD_ERROR_TIMEOUT:				return "Operation timed out";
	default:							return "Unknown error code";
	}
}

//////////////////////////////////////////////////////////////////////////////
// Communication with diagnostic MCU
//////////////////////////////////////////////////////////////////////////////

int pcd_mcu_clear(xildev *dev)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(uart_lite_clear(dev, UART_BASE_ADDRESS) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_mcu_tx(xildev *dev, uint8_t *tx_data, uint16_t num_bytes)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(uart_lite_tx(dev, UART_BASE_ADDRESS, tx_data, num_bytes) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_mcu_rx(xildev *dev, uint8_t *rx_data, uint16_t max_num_bytes)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	int result = uart_lite_rx(dev, UART_BASE_ADDRESS, rx_data, max_num_bytes);
	if(result < 0)
		return PCD_ERROR_REG_ACCESS;
	return result;
}

int pcd_mcu_set_dir(xildev *dev, int channel, bool actuator)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;

	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_CONTROL, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(actuator)
		reg_val |= 1 << channel;
	else
		reg_val &= ~(1 << channel);
	if(xil_write_reg(dev, REG_PCD_CONTROL, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_mcu_get_dir(xildev *dev, int channel, bool *actuator)
{
	if(!dev || !actuator)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;

	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_CONTROL, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*actuator = (reg_val & (1 << channel)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_mcu_is_actuator(xildev *dev, int channel, bool *actuator)
{
	if(!dev || !actuator)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;

	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*actuator = (reg_val & (1 << channel)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_mcu_get_prot(xildev *dev, int channel, bool *tripped)
{
	if(!dev || !tripped)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;

	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*tripped = (reg_val & (0x100 << channel)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_mcu_reset_prot(xildev *dev, int channel)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;

	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_CONTROL, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val |= 0x100 << channel;
	if(xil_write_reg(dev, REG_PCD_CONTROL, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	usleep(100000);
	reg_val &= ~(0x100 << channel);
	if(xil_write_reg(dev, REG_PCD_CONTROL, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Piezo-actuator functionality
//////////////////////////////////////////////////////////////////////////////

int pcd_act_set_trigger_source(xildev *dev, enum PcdTriggerLine trig_line, enum PcdTriggerLine stop_line, uint8_t evt_no)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	evt_no--;
	if(trig_line < 0 || trig_line > 7 || stop_line < 0 || stop_line > 7 || evt_no > 14)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = (trig_line & 7) | ((stop_line & 7) << 4) | ((evt_no & 15) << 8);
	if(xil_write_reg(dev, REG_GEN_TRIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_act_get_trigger_source(xildev *dev, enum PcdTriggerLine *trig_line, enum PcdTriggerLine *stop_line, uint8_t *evt_no)
{
	if(!dev || !trig_line || !stop_line || !evt_no)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_GEN_TRIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_line = reg_val & 7;
	*stop_line = (reg_val >> 4) & 7;
	*evt_no = ((reg_val >> 8) & 15) + 1;
	return PCD_ERROR_NONE;
}

int pcd_act_get_triggers_per_line(xildev *dev, uint8_t *trig_count)
{
	if(!dev || !trig_count)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_count = (reg_val >> 20) & 15;
	return PCD_ERROR_NONE;
}

int pcd_act_trigger_arm(xildev *dev, int channel, enum PcdTriggerType trig, bool auto_rearm)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val &= 0xFF0FFFFF; // trigger mode, bits 23..20
	if(xil_write_reg(dev, regs_base + ACT_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(trig == PCD_TR_FREE)
		reg_val |= 1 << ACT_BIT_FREE_RUN;
	if(trig == PCD_TR_ANY_EDGE || trig == PCD_TR_RISING_EDGE)
		reg_val |= 1 << ACT_BIT_RISING_EDGE;
	if(trig == PCD_TR_ANY_EDGE || trig == PCD_TR_FALLING_EDGE)
		reg_val |= 1 << ACT_BIT_FALLING_EDGE;
	if(auto_rearm)
		reg_val |= 1 << ACT_BIT_AUTO_REARM;
	if(xil_write_reg(dev, regs_base + ACT_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_act_get_trigger_type(xildev *dev, int channel, enum PcdTriggerType *trig, bool *auto_rearm)
{
	if(!dev || !trig || !auto_rearm)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(reg_val & (1 << ACT_BIT_FREE_RUN))
	{
		*trig = PCD_TR_FREE;
	}
	else
	{
		if(reg_val & (1 << ACT_BIT_RISING_EDGE))
		{
			if(reg_val & (1 << ACT_BIT_FALLING_EDGE))
				*trig = PCD_TR_ANY_EDGE;
			else
				*trig = PCD_TR_RISING_EDGE;
		}
		else
		{
			if(reg_val & (1 << ACT_BIT_FALLING_EDGE))
				*trig = PCD_TR_FALLING_EDGE;
			else
				*trig = PCD_TR_NONE;
		}
	}
	*auto_rearm = (reg_val & (1 << ACT_BIT_AUTO_REARM)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_act_was_triggered(xildev *dev, int channel, bool *triggered)
{
	if(!dev || !triggered)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*triggered = (reg_val & (1 << ACT_BIT_TRIG_DONE)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_act_set_sample_count(xildev *dev, int channel, uint16_t sample_count)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	if(sample_count > ACT_BUF_SAMPLES)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val &= 0xFFF00000;
	reg_val |= sample_count;
	if(xil_write_reg(dev, regs_base + ACT_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_act_get_sample_count(xildev *dev, int channel, uint16_t *sample_count)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*sample_count = reg_val & 0xFFFFF;
	return PCD_ERROR_NONE;
}

int pcd_act_set_trigger_delay(xildev *dev, int channel, double trig_delay)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	double delay_cycles = trig_delay * ACT_REF_FREQ;
	if(delay_cycles < 0.0 || delay_cycles > UINT32_MAX)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = round(delay_cycles);
	if(xil_write_reg(dev, regs_base + ACT_REG_DELAY, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_act_get_trigger_delay(xildev *dev, int channel, double *trig_delay)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t regs_base = gen_regs_ba[channel];
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, regs_base + ACT_REG_DELAY, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_delay = (double)reg_val / ACT_REF_FREQ;
	return PCD_ERROR_NONE;
}

int pcd_act_write_data(xildev *dev, int channel, int16_t *storage, uint32_t num_samples)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	if(num_samples > ACT_BUF_SAMPLES)
		return PCD_ERROR_OUT_OF_RANGE;
	int fd = xil_get_h2c_fd(dev, 0);
	if(fd <= 0)
		return PCD_ERROR_FILE_ACCESS;
	uint32_t req_bytes = num_samples * sizeof(*storage);
	uint8_t *buffer = (uint8_t*)storage;
	uint32_t sent_bytes = 0;
	lseek(fd, ACT_BUF_ADDRESS + channel * ACT_BUF_STEP, SEEK_SET);
	while(sent_bytes < req_bytes)
	{
		uint32_t bytes_to_write = req_bytes - sent_bytes;
		int res = write(fd, buffer, bytes_to_write);
		for(int i = 0; (i < WRITE_REPS) && (res < 0); i++)
		{
			res = write(fd, buffer, bytes_to_write);
			if(res == 0)
			break;
		}
		if(res < 0)
			return PCD_ERROR_FILE_ACCESS;
		else if(res == 0)
			usleep(10);
		sent_bytes += res;
		buffer += res;
	}
	return PCD_ERROR_NONE;
}

int pcd_act_dma_start(xildev *dev, int channel, uint32_t num_samples)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	if(num_samples > ACT_BUF_SAMPLES)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t dma_base = gen_dma_ba[channel];

	int ret;
	bool running, errors;
	ret = dma_mm2s_status(dev, dma_base, &running, &errors);
	if(ret != PCD_ERROR_NONE)
		return ret;

	if(running || errors)
	{
		ret = pcd_act_dma_stop(dev, channel);
		if(ret != PCD_ERROR_NONE)
			return ret;
	}

	int descr_no = 0x10 * (channel + 1);
	ret = dma_write_descriptor(dev, BRAM_BASE_ADDRESS, descr_no, descr_no,
				ACT_BUF_ADDRESS + ACT_BUF_STEP * channel, num_samples * sizeof(int16_t));
	if(ret != PCD_ERROR_NONE)
		return ret;

	ret = dma_mm2s_start(dev, dma_base, BRAM_BASE_ADDRESS, descr_no, descr_no + 1, true);
	if(ret != PCD_ERROR_NONE)
		return ret;
	return PCD_ERROR_NONE;
}

int pcd_act_dma_wait_idle(xildev *dev, int channel)
{
    if(!dev)
        return PCD_ERROR_NULL_POINTER;
    if(channel < 0 || channel >= ACT_CHANNELS)
        return PCD_ERROR_NO_SUCH_CHANNEL;
    uint32_t regs_base = gen_regs_ba[channel];
    uint32_t reg_val = 0;
    for(int rep = 100; rep > 0; rep--)
    {
        if(xil_read_reg(dev, regs_base + ACT_REG_STATUS, &reg_val) != 0)
            return PCD_ERROR_REG_ACCESS;
        if(reg_val & (1 << ACT_BIT_IDLE))
            return PCD_ERROR_NONE;
        usleep(1000);
    }
    return PCD_ERROR_TIMEOUT;
}

int pcd_act_dma_stop(xildev *dev, int channel)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(channel < 0 || channel >= ACT_CHANNELS)
		return PCD_ERROR_NO_SUCH_CHANNEL;
	uint32_t dma_base = gen_dma_ba[channel];
	uint32_t regs_base = gen_regs_ba[channel];

	uint32_t reg_val = 0;
	int ret;
	if(xil_read_reg(dev, regs_base + ACT_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val |= 1 << ACT_BIT_FLUSH;
	if(xil_write_reg(dev, regs_base + ACT_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;

	ret = dma_mm2s_stop(dev, dma_base);
	if(ret != PCD_ERROR_NONE)
		return ret;

	bool running, errors;
	for(int i = 0; i < 10; i++)
	{
		ret = dma_mm2s_status(dev, dma_base, &running, &errors);
		if(ret != PCD_ERROR_NONE)
			return ret;
		if(!running || errors)
			break;
		usleep(1000);
	}

	if(running || errors)
	{
		ret = dma_mm2s_reset(dev, SEN_DMA_BASE_ADDRESS);
		if(ret != PCD_ERROR_NONE)
			return ret;
	}

	reg_val &= ~(1 << ACT_BIT_FLUSH);
	if(xil_write_reg(dev, regs_base + ACT_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;

	return PCD_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Piezo-sensor functionality
//////////////////////////////////////////////////////////////////////////////

int pcd_sen_set_trigger_source(xildev *dev, enum PcdTriggerLine trig_line, enum PcdTriggerLine stop_line, uint8_t evt_no)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	evt_no--;
	if(trig_line < 0 || trig_line > 7 || stop_line < 0 || stop_line > 7 || evt_no > 14)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = (trig_line & 7) | ((stop_line & 7) << 4) | ((evt_no & 15) << 8);
	if(xil_write_reg(dev, REG_ACQ_TRIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_trigger_source(xildev *dev, enum PcdTriggerLine *trig_line, enum PcdTriggerLine *stop_line, uint8_t *evt_no)
{
	if(!dev || !trig_line || !stop_line || !evt_no)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_ACQ_TRIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_line = reg_val & 7;
	*stop_line = (reg_val >> 4) & 7;
	*evt_no = ((reg_val >> 8) & 15) + 1;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_triggers_per_line(xildev *dev, uint8_t *trig_count)
{
	if(!dev || !trig_count)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, REG_PCD_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_count = (reg_val >> 16) & 15;
	return PCD_ERROR_NONE;
}

int pcd_sen_trigger_arm(xildev *dev, enum PcdTriggerType trig, bool auto_rearm)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val &= 0xFF0FFFFF; // trigger mode, bits 23..20
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(trig == PCD_TR_FREE)
		reg_val |= 1 << SEN_BIT_FREE_RUN;
	if(trig == PCD_TR_ANY_EDGE || trig == PCD_TR_RISING_EDGE)
		reg_val |= 1 << SEN_BIT_RISING_EDGE;
	if(trig == PCD_TR_ANY_EDGE || trig == PCD_TR_FALLING_EDGE)
		reg_val |= 1 << SEN_BIT_FALLING_EDGE;
	if(auto_rearm)
		reg_val |= 1 << SEN_BIT_AUTO_REARM;
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_trigger_type(xildev *dev, enum PcdTriggerType *trig, bool *auto_rearm)
{
	if(!dev || !trig || !auto_rearm)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(reg_val & (1 << SEN_BIT_FREE_RUN))
	{
		*trig = PCD_TR_FREE;
	}
	else
	{
		if(reg_val & (1 << SEN_BIT_RISING_EDGE))
		{
			if(reg_val & (1 << SEN_BIT_FALLING_EDGE))
				*trig = PCD_TR_ANY_EDGE;
			else
				*trig = PCD_TR_RISING_EDGE;
		}
		else
		{
			if(reg_val & (1 << SEN_BIT_FALLING_EDGE))
				*trig = PCD_TR_FALLING_EDGE;
			else
				*trig = PCD_TR_NONE;
		}
	}
	*auto_rearm = (reg_val & (1 << SEN_BIT_AUTO_REARM)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_trigger_info(xildev *dev, uint8_t *trig_len, uint8_t *trig_cnt)
{
	if(!dev || !trig_len || !trig_cnt)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_cnt = (reg_val >> 24) & 0xFF;
	*trig_len = (reg_val >> 16) & 0xFF;
	return PCD_ERROR_NONE;
}

int pcd_sen_was_triggered(xildev *dev, bool *triggered)
{
	if(!dev || !triggered)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_STATUS, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*triggered = (reg_val & (1 << SEN_BIT_TRIG_DONE)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_sen_set_sample_count(xildev *dev, uint16_t sample_count)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(sample_count > SEN_MAX_SAMPLES)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val &= 0xFFF00000;
	reg_val |= sample_count;
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_sample_count(xildev *dev, uint16_t *sample_count)
{
	if(!dev || !sample_count)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*sample_count = reg_val & 0xFFFFF;
	return PCD_ERROR_NONE;
}

int pcd_sen_set_trigger_delay(xildev *dev, double trig_delay)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	double delay_cycles = trig_delay * SEN_REF_FREQ;
	if(delay_cycles < 0.0 || delay_cycles > UINT32_MAX)
		return PCD_ERROR_OUT_OF_RANGE;
	uint32_t reg_val = round(delay_cycles);
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_DELAY, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_trigger_delay(xildev *dev, double *trig_delay)
{
	if(!dev || !trig_delay)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_DELAY, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*trig_delay = (double)reg_val / SEN_REF_FREQ;
	return PCD_ERROR_NONE;
}

int pcd_sen_set_pre_trigger(xildev *dev, bool enabled)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(enabled)
		reg_val |= 1 << SEN_BIT_PRE_TRIG;
	else
		reg_val &= ~(1 << SEN_BIT_PRE_TRIG);
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_pre_trigger(xildev *dev, bool *enabled)
{
	if(!dev || !enabled)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_CONFIG, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*enabled = (reg_val & (1 << SEN_BIT_PRE_TRIG)) != 0;
	return PCD_ERROR_NONE;
}

int pcd_sen_set_sample_dropping(xildev *dev, uint8_t samples_to_drop)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_MISC, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	reg_val &= 0xFF00FFFF;
	reg_val |= samples_to_drop << 16;
	if(xil_write_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_MISC, reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int pcd_sen_get_sample_dropping(xildev *dev, uint8_t *samples_to_drop)
{
	if(!dev || !samples_to_drop)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, SEN_TRIG_BASE_ADDRESS + SEN_REG_MISC, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*samples_to_drop = (reg_val >> 16) & 0xFF;
	return PCD_ERROR_NONE;
}

int pcd_sen_read_prepare(xildev *dev, uint32_t num_samples)
{
	int ret;
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(num_samples == 0)
		return PCD_ERROR_OUT_OF_RANGE;

	ret = dma_write_descriptor(dev, BRAM_BASE_ADDRESS, 0, 0, SEN_BUF_ADDRESS, num_samples * sizeof(PcdSensorSample));
	if(ret != PCD_ERROR_NONE)
		return ret;

	ret = dma_s2mm_start(dev, SEN_DMA_BASE_ADDRESS, BRAM_BASE_ADDRESS, 0, 0, false);
	if(ret != PCD_ERROR_NONE)
		return ret;

	return PCD_ERROR_NONE;
}

int pcd_sen_read_check(xildev *dev, bool *running)
{
	int ret;
	if(!dev || !running)
		return PCD_ERROR_NULL_POINTER;

	bool errors;
	ret = dma_s2mm_status(dev, SEN_DMA_BASE_ADDRESS, running, &errors);
	if(ret != PCD_ERROR_NONE)
		return ret;
	if(errors)
		return PCD_ERROR_DMA_TRANSFER_FAILED;

	return PCD_ERROR_NONE;
}

int pcd_sen_read_finalize(xildev *dev)
{
	int ret;
	if(!dev)
		return PCD_ERROR_NULL_POINTER;

	bool errors, running;
	ret = dma_s2mm_status(dev, SEN_DMA_BASE_ADDRESS, &running, &errors);
	if(ret != PCD_ERROR_NONE)
		return ret;

	ret = dma_s2mm_stop(dev, SEN_DMA_BASE_ADDRESS);
	if(ret != PCD_ERROR_NONE)
		return ret;

	if(running)
	{
		ret = dma_s2mm_reset(dev, SEN_DMA_BASE_ADDRESS);
		if(ret != PCD_ERROR_NONE)
			return ret;
		return PCD_ERROR_DMA_TRANSFER_FAILED;
	}

	return PCD_ERROR_NONE;
}

int pcd_sen_read_transfer(xildev *dev, PcdSensorSample *storage, uint32_t num_samples)
{
	if(!dev || !storage)
		return PCD_ERROR_NULL_POINTER;
	if(num_samples == 0)
		return PCD_ERROR_OUT_OF_RANGE;

	int fd = xil_get_c2h_fd(dev, 0);
	if(fd <= 0)
		return PCD_ERROR_FILE_ACCESS;
	uint32_t req_bytes = num_samples * sizeof(PcdSensorSample);
	uint8_t *buffer = (uint8_t*)storage;
	uint32_t rcvd_bytes = 0;
	lseek(fd, SEN_BUF_ADDRESS, SEEK_SET);

	while(rcvd_bytes < req_bytes)
	{
		uint32_t bytes_to_read = req_bytes - rcvd_bytes;
		int res = read(fd, buffer, bytes_to_read);
		if(res < 0)
			return PCD_ERROR_FILE_ACCESS;
		else if(res == 0)
			usleep(10);
		rcvd_bytes += res;
		buffer += res;
	}
	return PCD_ERROR_NONE;
}

int pcd_sen_read_data(xildev *dev, PcdSensorSample *storage, uint32_t num_samples, unsigned int timeout)
{
	int ret;
	if(!dev || !storage)
		return PCD_ERROR_NULL_POINTER;
	if(num_samples == 0)
		return PCD_ERROR_OUT_OF_RANGE;

	ret = pcd_sen_read_prepare(dev, num_samples);
	if(ret != PCD_ERROR_NONE)
		return ret;

	for(unsigned int i = 0; i <= timeout; i++)
	{
		bool running;
		ret = pcd_sen_read_check(dev, &running);
		if(ret != PCD_ERROR_NONE)
			return ret;
		if(!running)
			break;
		usleep(1000);
	}

	ret = pcd_sen_read_finalize(dev);
	if(ret != PCD_ERROR_NONE)
		return ret;

	ret = pcd_sen_read_transfer(dev, storage, num_samples);
	if(ret != PCD_ERROR_NONE)
		return ret;

	return PCD_ERROR_NONE;
}
