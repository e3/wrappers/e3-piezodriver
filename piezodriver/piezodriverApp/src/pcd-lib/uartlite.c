#include "uartlite.h"
#include <time.h>
#include <stdio.h>

enum XilUartLiteReg
{
	REG_RXFIFO	= 0x00,
	REG_TXFIFO	= 0x04,
	REG_STAT	= 0x08,
	REG_CTRL	= 0x0C
};

enum XilUartLiteStatusBit
{
	BIT_RX_VALID		= 0,
	BIT_RX_FULL			= 1,
	BIT_TX_EMPTY		= 2,
	BIT_TX_FULL			= 3,
	BIT_INT_ENABLED		= 4,
	BIT_OVERRUN_ERR		= 5,
	BIT_FRAME			= 6,
	BIT_PARITY			= 7
};

enum XilUartLiteControlBit
{
	BIT_TX_RST			= 0,
	BIT_RX_RST			= 1
};

int uart_lite_clear(xildev *dev, uint32_t base_addr)
{
	if(xil_write_reg(dev, base_addr + REG_CTRL, (1 << BIT_TX_RST) | (1 << BIT_RX_RST)) != 0)
		return -1;
	if(xil_write_reg(dev, base_addr + REG_CTRL, 0) != 0)
		return -2;
	return 0;
}

int uart_lite_tx(xildev *dev, uint32_t base_addr, uint8_t * tx_data, uint16_t num_bytes)
{
	struct timespec st;
	for(uint16_t i = 0; i < num_bytes; i++)
	{
		int timeout = 1000;
		while(timeout)
		{
			uint32_t status_reg;
			if(xil_read_reg(dev, base_addr + REG_STAT, &status_reg) != 0)
				return -1;
			if(!(status_reg & (1 << BIT_TX_FULL)))
				break;

			st.tv_sec = 0;
			st.tv_nsec = 100000L; // 100 us
			nanosleep(&st, NULL);
			timeout--;
		}
		if(!timeout)
			return -2;
		if(xil_write_reg(dev, base_addr + REG_TXFIFO, tx_data[i]) != 0)
			return -3;
		st.tv_sec = 0;
		st.tv_nsec = 10000000L; // 10 ms
		nanosleep(&st, NULL);
	}
	return 0;
}

int uart_lite_rx(xildev *dev, uint32_t base_addr, uint8_t * rx_data, uint16_t max_num_bytes)
{
	uint16_t num_bytes;
	for(num_bytes = 0; num_bytes < max_num_bytes; num_bytes++)
	{
		uint32_t status_reg, data_rx_reg;
		if(xil_read_reg(dev, base_addr + REG_STAT, &status_reg) != 0)
			return -1;
		if(!(status_reg & (1 << BIT_RX_VALID)))
			break;
		if(xil_read_reg(dev, base_addr + REG_RXFIFO, &data_rx_reg) != 0)
			return -2;
		rx_data[num_bytes] = data_rx_reg;
	}
	return num_bytes;
}
