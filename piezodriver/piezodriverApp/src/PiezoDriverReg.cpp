#include <stdlib.h>

#include <iocsh.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>

#include "PiezoDriverAsynPortDriver.h"


/* Configuration routine.  Called directly, or from the iocsh function below */
extern "C" {


  /** EPICS iocsh callable function to call constructor for the PiezoDriverAsynPortDriver class.
    * \param[in] portName The name of the asyn port driver to be created.
    * \param[in] maxPoints The maximum  number of points in the volt and time arrays */

  PiezoDriverAsynPortDriver *drv ;

  void exitFunc(void*)
  {
    drv->TerminateAllThreads() ;
    delete drv ;
  }

  int PiezoDriverAsynPortDriverConfigure(const char *portName, int maxPoints, const char *devName)
  {
    static int onlyOnce = 0 ;

    if ( onlyOnce ) return asynSuccess ;
    onlyOnce = 1 ;

    epicsAtExit( exitFunc, 0 ) ;

    drv = new PiezoDriverAsynPortDriver(portName, maxPoints, devName);
    return(asynSuccess);
  }

  /* EPICS iocsh shell commands */
  static const iocshArg initArg0 = { "portName",iocshArgString};
  static const iocshArg initArg1 = { "max points",iocshArgInt};
  static const iocshArg initArg2 = { "dev name", iocshArgString};
  static const iocshArg * const initArgs[] = {&initArg0, &initArg1, &initArg2};

  static const iocshFuncDef initFuncDef = {"PiezoDriverAsynPortDriverConfigure",3,initArgs};

  static void initCallFunc(const iocshArgBuf *args)
  {
    PiezoDriverAsynPortDriverConfigure(args[0].sval, args[1].ival, args[2].sval);
  }

  void PiezoDriverAsynPortDriverRegister(void)
  {
    iocshRegister(&initFuncDef,initCallFunc);
  }

  epicsExportRegistrar(PiezoDriverAsynPortDriverRegister);
}
