#include "epicsThread.h"
#include "PiezoDriverAsynPortDriver.h"

using namespace std;

void PiezoDriverDiag(void *drvPvt)
{
  PiezoDriverAsynPortDriver *pPvt = (PiezoDriverAsynPortDriver *)drvPvt;
  pPvt->PiezoDriverDiag();
}

void PiezoDriverAsynPortDriver::PiezoDriverDiag(void)
{
  epicsMutexMustLock(terminateWorkersLock);
  asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "Starting Diagnostic thread\n");

  while (terminateWorkers==0)
  {
    epicsMutexUnlock(terminateWorkersLock);

    if (deviceConnected) {
      int res = diag.readData();
      if (res != 0)
      {
        asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "Error while reading data from diagnostic CPU\n");
      }
      else
      {
        lock();
        setDoubleParam(P_CHAN_A_DIAG_CPU.VOLT_AC_RD, diag.chanA.voltageAC);
        setDoubleParam(P_CHAN_A_DIAG_CPU.VOLT_DC_RD, diag.chanA.voltageDC);
        setDoubleParam(P_CHAN_A_DIAG_CPU.VOLT_RMS_RD, diag.chanA.voltageRMS);
        setDoubleParam(P_CHAN_A_DIAG_CPU.CUR_AC_RD, diag.chanA.currentAC);
        setDoubleParam(P_CHAN_A_DIAG_CPU.CUR_DC_RD, diag.chanA.currentDC);
        setDoubleParam(P_CHAN_A_DIAG_CPU.CUR_RMS_RD, diag.chanA.currentRMS);
        setDoubleParam(P_CHAN_A_DIAG_CPU.RESISTOR_RD, diag.chanA.idResistor);
        setDoubleParam(P_CHAN_A_DIAG_CPU.TEMP_RD, diag.chanA.temperature);
        setStringParam(P_CHAN_A_DIAG_CPU.MODE_RD, diag.chanA.mode);
        setStringParam(P_CHAN_A_DIAG_CPU.STATUS_RD, diag.chanA.status);
        setStringParam(P_CHAN_A_DIAG_CPU.DETAILED_STATUS_RD, diag.chanA.getDetailedStatus());
        setStringParam(P_CHAN_A_DIAG_CPU.POW_AMP_RD, diag.chanA.paFlags);
        setStringParam(P_CHAN_A_DIAG_CPU.DETAILED_POW_AMP_RD, diag.chanA.getDetailedPAStatus());
        setDoubleParam(P_CHAN_B_DIAG_CPU.VOLT_AC_RD, diag.chanB.voltageAC);
        setDoubleParam(P_CHAN_B_DIAG_CPU.VOLT_DC_RD, diag.chanB.voltageDC);
        setDoubleParam(P_CHAN_B_DIAG_CPU.VOLT_RMS_RD, diag.chanB.voltageRMS);
        setDoubleParam(P_CHAN_B_DIAG_CPU.CUR_AC_RD, diag.chanB.currentAC);
        setDoubleParam(P_CHAN_B_DIAG_CPU.CUR_DC_RD, diag.chanB.currentDC);
        setDoubleParam(P_CHAN_B_DIAG_CPU.CUR_RMS_RD, diag.chanB.currentRMS);
        setDoubleParam(P_CHAN_B_DIAG_CPU.RESISTOR_RD, diag.chanB.idResistor);
        setDoubleParam(P_CHAN_B_DIAG_CPU.TEMP_RD, diag.chanB.temperature);
        setStringParam(P_CHAN_B_DIAG_CPU.MODE_RD, diag.chanB.mode);
        setStringParam(P_CHAN_B_DIAG_CPU.STATUS_RD, diag.chanB.status);
        setStringParam(P_CHAN_B_DIAG_CPU.DETAILED_STATUS_RD, diag.chanB.getDetailedStatus());
        setStringParam(P_CHAN_B_DIAG_CPU.POW_AMP_RD, diag.chanB.paFlags);
        setStringParam(P_CHAN_B_DIAG_CPU.DETAILED_POW_AMP_RD, diag.chanB.getDetailedPAStatus());
        unlock();

        callParamCallbacks();
      }
    }

    epicsThreadSleep(5.0);

    epicsMutexMustLock(terminateWorkersLock);
  }
  epicsMutexUnlock(terminateWorkersLock);

  // termination handling
  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}
