
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS = linux-ppc64e6500

USR_CFLAGS   += -std=c99

SRC_BASE:=piezodriverApp/src

USR_INCLUDES += -I$(where_am_I)$(SRC_BASE)

HEADERS += $(SRC_BASE)/PiezoDriverAsynPortDriver.h

HEADERS += $(SRC_BASE)/piezo/PiezoSensor.h
HEADERS += $(SRC_BASE)/piezo/PiezoActuator.h

HEADERS += $(SRC_BASE)/piezo/DiagnosticProcessor.h

HEADERS += $(SRC_BASE)/pcd-lib/libpcd.h
HEADERS += $(SRC_BASE)/pcd-lib/dma.h
HEADERS += $(SRC_BASE)/pcd-lib/uartlite.h

SOURCES += $(SRC_BASE)/pcd-lib/libpcd.c
SOURCES += $(SRC_BASE)/pcd-lib/dma.c
SOURCES += $(SRC_BASE)/pcd-lib/uartlite.c

SOURCES += $(SRC_BASE)/PiezoDriverAsynPortDriver.cpp
SOURCES += $(SRC_BASE)/PiezoDriverDiagnostic.cpp
SOURCES += $(SRC_BASE)/PiezoDriverIrq.cpp
SOURCES += $(SRC_BASE)/PiezoDriverReg.cpp

SOURCES += $(SRC_BASE)/piezo/PiezoSensor.cpp
SOURCES += $(SRC_BASE)/piezo/PiezoActuator.cpp

SOURCES += $(SRC_BASE)/piezo/DiagnosticProcessor.cpp

DBDS    += $(SRC_BASE)/PiezoDriverAsynPortDriverInclude.dbd

SCRIPTS += $(wildcard iocsh/*.iocsh)

TEMPLATES += $(wildcard  $(SRC_BASE)/../Db/*.db)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db

vlibs:

.PHONY: vlibs
